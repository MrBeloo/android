package com.example.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by dev2 on 31.01.14.
 */
public class Fragment4 extends Fragment {

    final String LOG = "beloo";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "Fragment4 onCreateView");

        View v = inflater.inflate(R.layout.fragment, container, false);
        ((TextView)v.findViewById(R.id.textView_num)).setText("fourth fragment");

        return v;
    }
}
