package com.example.app;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by dev2 on 31.01.14.
 */
public class FirstFragment extends Fragment{
    final String LOG = "beloo";

    EditText someText;
    Button sendText_bu;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "Fragment1 onCreateView");

        View v = inflater.inflate(R.layout.first_fragment, null);
        assert v != null;
        someText = (EditText) v.findViewById(R.id.editText_some);
        sendText_bu = (Button) v.findViewById(R.id.button_text);

        sendText_bu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView tw;
                try {       // not works by another way.
                    Fragment f2 = getActivity().getFragmentManager().findFragmentById(R.id.linear_two);
                    tw = (TextView)f2.getView().findViewById(R.id.textview_get);
                }
                catch (NullPointerException e){
                    Fragment f2 = getActivity().getFragmentManager().findFragmentById(R.id.fragment2);
                    tw = (TextView)f2.getView().findViewById(R.id.textview_get);
                }
                tw.setText(someText.getText());
                tw.setVisibility(View.VISIBLE);
            }
        });

        return v;
    }
}
