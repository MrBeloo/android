package com.example.app;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by dev2 on 31.01.14.
 */
public class SecondFragment extends Fragment {
    final String LOG = "beloo";

    Button newActivity_bu;
    TextView textView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "Fragment2 onCreateView");

        View v = inflater.inflate(R.layout.second_fragment, null);

        newActivity_bu = (Button) v.findViewById(R.id.button_activity);
        textView = (TextView) v.findViewById(R.id.textview_get);

        newActivity_bu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }
}
