package com.example.app;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev2 on 31.01.14.
 */
public class ForthFragment extends Fragment {

    final String LOG = "beloo";
    final int ID = 3;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "Fragment4 onCreateView");

        View v = inflater.inflate(R.layout.fragment_4, null);
        Button b = (Button)(v.findViewById(R.id.button_replace)); // по заданию все в коде
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).onClick(ID);
            }
        });

        ((Button)(v.findViewById(R.id.button_to2))).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack(1, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });

        return v;
    }

    @Override
    public String toString() {
        return "ForthFragment";
    }
}
