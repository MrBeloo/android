package com.example.app;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by dev2 on 31.01.14.
 */
public class SecondFragment extends Fragment {

    final String LOG = "beloo";
    final int ID = 1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG, "Fragment2 onCreateView");

        View v = inflater.inflate(R.layout.fragment_2, null);
        Button b = (Button)(v.findViewById(R.id.button_replace)); // по заданию все в коде
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).onClick(ID);
            }
        });

        return v;
    }

    @Override
    public String toString() {
        return "SecondFragment";
    }
}
