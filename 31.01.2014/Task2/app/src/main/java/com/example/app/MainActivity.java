package com.example.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends Activity{

    final String LOG = "beloo";

    ArrayList<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        fragments = new ArrayList<Fragment>(Arrays.asList(new FirstFragment(), new SecondFragment(), new ThirdFragment(), new ForthFragment()));

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragments.get(0))
                    .commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onClick(int fragmentId) {

        switch (fragmentId){
            case 0:
            case 1:
            case 2:
                getFragmentManager().beginTransaction()
                    .replace(R.id.container, fragments.get(fragmentId+1))
                    .addToBackStack(null)
                    .commit();
                break;
            case 3:
                getFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                break;
        }


        Log.d(LOG, "onclick");

    }
}
