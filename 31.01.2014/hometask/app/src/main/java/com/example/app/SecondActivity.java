package com.example.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SecondActivity extends FragmentActivity {

    final static String LOG = "beloo";
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    public static class PlaceholderFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_second, container, false);
            return rootView;
        }
    }

    public void onClick(View view){
        progress = new ProgressDialog(this);
        progress.setMessage("Загрузка..");
        progress.show();
        new AsyncTaskLoadHtml().execute("http://google.com/");
    }


    private class AsyncTaskLoadHtml extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... value) {
            StringBuilder page = new StringBuilder();
            try{
                URL url = new URL(value[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream connStream = connection.getInputStream();
                Log.d(LOG,"after connect");

                InputStreamReader reader = new InputStreamReader(connStream);
                char [] buffer = new char[8192];
                int read;
                Log.d(LOG,"before load");
                try {
                    while ((read = reader.read(buffer)) > 0) {
                        onProgressUpdate();
                        page.append(buffer, 0, read);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(LOG,"load html part error");
                }
            } catch (MalformedURLException e) {
                Log.d(LOG,"url error");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d(LOG,"IO exception");
                e.printStackTrace();
            }

            return page.toString();
        }

        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            Log.d(LOG,"progress");
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            progress.dismiss();
            Log.d(LOG,"send to activity");
            Intent intent = new Intent(SecondActivity.this, MainActivity.class);
            intent.putExtra("get_html", result);
            Log.d(LOG, result);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
