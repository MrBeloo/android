package task1.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private AsyncTaskFactorial task;
    private EditText value_text;
    private TextView factorial_text;
    private TextView factorial_label;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        value_text = (EditText) findViewById(R.id.editText_number);
        factorial_text = (TextView) findViewById(R.id.textView_factorial);
        factorial_label = (TextView) findViewById(R.id.textView2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class AsyncTaskFactorial extends AsyncTask<Long, Void, Long> {

        @Override
        protected Long doInBackground(Long... value) {
            Long result = 1l;
            for (int i=1; i<=value[0]; i++){
                result*=i;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);

            factorial_text.setText(result.toString());
            factorial_label.setVisibility(View.VISIBLE);
        }
    }

    public void onClickCalcButton (View view){
        if (!value_text.getText().toString().matches("")) {
            if (task == null || task.getStatus().equals(AsyncTask.Status.FINISHED)) {
                task = new AsyncTaskFactorial();
                task.execute(Long.parseLong(value_text.getText().toString()));
            } else {
                Toast.makeText(MainActivity.this, "Поток не завершен", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

}
