package com.example.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.app.LoaderManager.LoaderCallbacks;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends Activity implements LoaderCallbacks<Long>{

    final String LOG = "Logs";

    private EditText value_text;
    private TextView factorial_text;
    private TextView factorial_label;

    private static final int LOADER_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        value_text = (EditText) findViewById(R.id.editText_number);
        factorial_text = (TextView) findViewById(R.id.textView_factorial);
        factorial_label = (TextView) findViewById(R.id.textView2);

        Bundle bndl = new Bundle();
        getLoaderManager().initLoader(LOADER_ID, bndl, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onClickCalcButton (View view){


        if (!value_text.getText().toString().matches("")){
            Log.d(LOG, hashCode() + " click button");

            Bundle bndl = new Bundle();
            bndl.putLong(FactorialLoader.KEY_VALUE, Long.parseLong(value_text.getText().toString()));
            getLoaderManager().restartLoader(LOADER_ID, bndl,this).forceLoad();
            Log.d(LOG, "main thread");

        }
    }

    @Override
    public Loader<Long> onCreateLoader(int i, Bundle bundle) {
        Loader<Long> loader = null;
        if (i == LOADER_ID) {
            loader = new FactorialLoader(this, bundle);
            Log.d(LOG, "onCreateLoader: " + loader.hashCode());
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Long> longLoader, Long aLong) {
        Log.d(LOG, "onLoaderFinish: " + aLong);
        factorial_label.setVisibility(View.VISIBLE);
        factorial_text.setText(aLong.toString());
    }

    @Override
    public void onLoaderReset(Loader<Long> longLoader) {
        Log.d(LOG, "onLoadReset: ");
    }
}

