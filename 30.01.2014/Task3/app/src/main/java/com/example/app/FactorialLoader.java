package com.example.app;

import android.content.Context;
import android.content.Loader;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by dev2 on 30.01.14.
 */

public class FactorialLoader extends Loader {

    public final static String KEY_VALUE = "input_value";
    final String LOG = "Logs";

    private long result;
    private long value;

    public FactorialLoader(Context context, Bundle args) {
        super(context);
        Log.d(LOG, "loader constructor");
        value = args.getLong(KEY_VALUE);
    }

    protected void onForceLoad() {
        super.onForceLoad();
        result = 1l;
        for (int i=1; i<=value; i++){
            result*=i;
        }
        Log.d(LOG, result + " onForceLoad");
        deliverResult(result);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(LOG, hashCode() + " onStartLoading");
        if (takeContentChanged())
            forceLoad();
    }
}

