package task1.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private EditText value_text;
    private TextView factorial_text;
    private TextView factorial_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        value_text = (EditText) findViewById(R.id.editText_number);
        factorial_text = (TextView) findViewById(R.id.textView_factorial);
        factorial_label = (TextView) findViewById(R.id.textView2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onClickCalcButton (View view){

        if (!value_text.getText().toString().matches("")){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Long value = Long.parseLong(value_text.getText().toString());
                    Long result = 1l;
                    for (int i=1; i<=value; i++){
                        result*=i;
                    }

                    final Long fResult = result;

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            factorial_text.setText(fResult.toString());
                            factorial_label.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }).start();
        }
    }

}
