package com.example.app;


import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    private AsyncCounter task;
    private static TextView factorial_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        factorial_text = (TextView) findViewById(R.id.textView_factorial);

        task = (AsyncCounter) getLastNonConfigurationInstance();

        if (task != null) {
            task.link(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private static class AsyncCounter extends AsyncTask<Integer, Integer, Void> {
        MainActivity activity;

        void link(MainActivity act) {
            activity = act;
        }

        void unLink() {
            activity = null;
        }

        @Override
        protected Void doInBackground(Integer... params) {

            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(i);
                Log.d("my_log", "i = " + i);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            factorial_text.setText("i = " + values[0]);
        }
    }

    public void onClickCalcButton (View view){
        if (task == null || task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task = new AsyncCounter();
            task.execute();
        }
    }

    public Object onRetainNonConfigurationInstance() {
        task.unLink();
        return task;
    }

}


