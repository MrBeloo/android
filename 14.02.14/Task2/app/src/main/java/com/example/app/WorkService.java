package com.example.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class WorkService extends Service {

    public static int RESULT_CODE = 1;
    CountThread thread;
    NotificationManager notyManager;
    private static int ONGOING_NOTIFICATION = 1;

    public WorkService() {
        count = 0;
    }

    private int count;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        notyManager =(NotificationManager) getApplicationContext()
                .getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (thread == null || !thread.isAlive()){
            PendingIntent pIntent = PendingIntent.getActivity(this, 0, new Intent(), 0);
            Notification notif = new NotificationCompat.Builder(WorkService.this)
                    .setContentTitle("count")
                    .setContentText(String.valueOf(count))
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentIntent(pIntent)
                    .build();

            startForeground(ONGOING_NOTIFICATION,notif);


            PendingIntent pi = intent.getParcelableExtra("pi");
            thread = new CountThread(pi);
            thread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }


    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        thread.interrupt();
    }



    private class CountThread extends Thread{

        PendingIntent pi;

        public CountThread (PendingIntent pi){
            this.pi = pi;
        }

        @Override
        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()){
                    TimeUnit.SECONDS.sleep(1);
                    count++;
                    Log.d(Prefs.LOG, ""+count);
                    Intent intent =  new Intent();
                    intent.putExtra(MainActivity.ACCEPT, count);
                    pi.send(WorkService.this, RESULT_CODE , intent);

                    PendingIntent pIntent = PendingIntent.getActivity(WorkService.this, 0, new Intent(), 0);

                    Notification notif = new NotificationCompat.Builder(WorkService.this)
                            .setContentTitle("count")
                            .setContentText(String.valueOf(count))
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentIntent(pIntent)
                            .build(); // available from API level 11 and onwards

                    notif.flags |= Notification.FLAG_AUTO_CANCEL;
                    notyManager.notify(ONGOING_NOTIFICATION, notif);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();

            }
        }
    }
}
