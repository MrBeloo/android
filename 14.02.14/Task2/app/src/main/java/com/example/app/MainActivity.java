package com.example.app;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private final static int INTENT1 = 100;
    public final static String ACCEPT = "accept";

    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bu_start = (Button)findViewById(R.id.button_start);
        Button bu_stop = (Button)findViewById(R.id.button_stop);
        result = (TextView)findViewById(R.id.textView_result);

        bu_start.setOnClickListener(this);
        bu_stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, WorkService.class);
        switch(view.getId()){
            case R.id.button_start:
                PendingIntent pi = createPendingResult(INTENT1, new Intent(), 0);
                i.putExtra("pi", pi);
                startService(i);
                break;
            case R.id.button_stop:
                stopService(i);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(Prefs.LOG, "resultAccepted");
        switch (requestCode){
            case INTENT1:
                int resi = data.getIntExtra(ACCEPT, 0);
                result.setText(String.valueOf(resi));
                break;
        }
    }
}
