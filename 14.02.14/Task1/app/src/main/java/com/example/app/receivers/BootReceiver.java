package com.example.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.app.WorkService;

import java.util.Random;

public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, WorkService.class);
        Random rnd = new Random();
        int randomInput = rnd.nextInt(20);
        serviceIntent.putExtra("value", randomInput);
        context.startService(serviceIntent);
    }
}
