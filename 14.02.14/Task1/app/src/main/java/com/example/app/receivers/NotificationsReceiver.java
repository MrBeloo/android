package com.example.app.receivers;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.app.Prefs;
import com.example.app.R;

public class  NotificationsReceiver extends BroadcastReceiver {

    NotificationManager notyManager;

    public NotificationsReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(Prefs.LOG,"send notification");
        notyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);

        Notification notif = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.factorial))
                .setContentText(String.valueOf(intent.getLongExtra("result", 0)))
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .build(); // available from API level 11 and onwards



        notif.flags |= Notification.FLAG_AUTO_CANCEL;
        notyManager.notify(1, notif);
    }
}
