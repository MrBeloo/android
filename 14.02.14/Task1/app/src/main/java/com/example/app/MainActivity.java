package com.example.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;

public class MainActivity extends ActionBarActivity implements View.OnClickListener, IServiceListener{

    private EditText input;
    public static TextView result;

    public final static String PARAM_RESULT = "serviceRes";
    public final static int INTENT1 = 1000;

    private ServiceConnection connection;
    private WorkService service;
    private AlarmManager alarm;
    private BroadcastReceiver broadcastReceiver;
    private Boolean registered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText)findViewById(R.id.editText_input);
        Button bu_noBind = (Button)findViewById(R.id.button_noBind);
        Button bu_localBind = (Button)findViewById(R.id.button_localBinding);
        Button bu_intentService = (Button)findViewById(R.id.button_intentService);
        Button bu_broadcast = (Button)findViewById(R.id.button_broadcast);
        Button bu_pending = (Button)findViewById(R.id.button_pending);
        result = (TextView)findViewById(R.id.textView_result);
        alarm = (AlarmManager)getSystemService(ALARM_SERVICE);

        bu_noBind.setOnClickListener(this);
        bu_localBind.setOnClickListener(this);
        bu_intentService.setOnClickListener(this);
        bu_broadcast.setOnClickListener(this);
        bu_pending.setOnClickListener(this);

        connection = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                service = ((WorkService.ServiceBinder)iBinder).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                service = null;
            }
        };

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(Prefs.LOG, "activity broadcast received");
                Long factorial = intent.getLongExtra("result",0);
                result.setText(getString(R.string.result)+" " + factorial);
                abortBroadcast();
            }
        };

    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, WorkService.class), connection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, WorkService.class);
        if (!input.getText().toString().equals("")){
            int inputNumber = Integer.parseInt(input.getText().toString());
            switch (view.getId()){
                case R.id.button_localBinding:
                    if (service!=null){
                        service.calculate(inputNumber, this);
                    }
                    break;
                case R.id.button_noBind:
                    IntentFilter filter = new IntentFilter("action.fromService");
                    filter.setPriority(0);
                    registerReceiver(broadcastReceiver, filter);
                    break;
                case R.id.button_pending:
                    PendingIntent pi = createPendingResult(INTENT1, new Intent(), 0);
                    i.putExtra("pi", pi);
                    break;
                case R.id.button_intentService:
                    PendingIntent pi2 = createPendingResult(INTENT1, new Intent(), 0);
                    i = new Intent(this, WorkIntentService.class);
                    i.putExtra("pi", pi2);
                    break;
                case R.id.button_broadcast:
                    Log.d(Prefs.LOG,"broadcast button clicked");
                    i = new Intent();
                    i.setAction("action.serviceBroadcast");
                    i.putExtra("value", inputNumber);
                    Log.d("beloo",""+inputNumber);
                    PendingIntent broadcastIntent = PendingIntent.getBroadcast(this, INTENT1, i, PendingIntent.FLAG_CANCEL_CURRENT);

                    alarm.set(AlarmManager.RTC, System.currentTimeMillis() + 10000, broadcastIntent);
                    return;
            }
            i.putExtra("value", inputNumber);
            startService(i);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case INTENT1:
                long resi = data.getLongExtra(PARAM_RESULT, 0);
                result.setText(getString(R.string.result)+" " + resi);
                break;
        }
    }

    @Override
    public void factorialCalculated(long factorial) {
        result.setText(getString(R.string.result)+" " + factorial);
    }
}