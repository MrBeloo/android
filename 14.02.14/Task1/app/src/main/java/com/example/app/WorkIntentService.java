package com.example.app;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;

public class WorkIntentService extends IntentService {

    public WorkIntentService() {
        super(WorkIntentService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int inputNumber = intent.getIntExtra("value",0);
        PendingIntent pi = intent.getParcelableExtra("pi");

        long factorial = 1;
        for (long i=1; i<=inputNumber; i++){
            factorial*=i;
        }
        if (pi!=null){
            try {
                intent.putExtra(MainActivity.PARAM_RESULT, factorial);
                pi.send(this, 0 , intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
    }
}
