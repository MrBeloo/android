package com.example.app;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dev2 on 2/14/14.
 */
public class WorkService extends Service{

    private ExecutorService executor;
    private ServiceBinder binder = new ServiceBinder();


    @Override
    public void onCreate() {
        super.onCreate();
        executor = Executors.newFixedThreadPool(3);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int inputNumber = intent.getIntExtra("value", 0);
        PendingIntent pi = intent.getParcelableExtra("pi");
        IServiceListener listener = (IServiceListener)intent.getSerializableExtra("listener");
        executor.execute(new CalcFactorial(inputNumber, pi, new ServiceHandler(null)));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void calculate(int inputNumber, final IServiceListener listener){
        executor.execute(new CalcFactorial(inputNumber, null, new ServiceHandler(listener)));
    }

    class ServiceBinder extends Binder {
        WorkService getService() {
            return WorkService.this;
        }

    }

    private class ServiceHandler extends Handler{

        private IServiceListener listener;

        public ServiceHandler(IServiceListener listener) {
            this.listener = listener;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Long factorial = msg.getData().getLong("result");
            if (listener!= null)
                listener.factorialCalculated(factorial);
            else {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("action.fromService");
                broadcastIntent.putExtra("result", factorial);
                sendOrderedBroadcast(broadcastIntent, null);
            }
        }
    }

    private class CalcFactorial extends Thread{

        private int inputNumber;
        private PendingIntent pi;
        private Handler handler;

        public CalcFactorial(int inputNumber, PendingIntent pi, Handler handler) {
            this.inputNumber = inputNumber;
            this.pi = pi;
            this.handler = handler;
        }

        @Override
        public void run() {
            long factorial = 1;
            for (long i=1; i<=inputNumber; i++){
                factorial*=i;
            }
            if (pi!=null){
                try {
                    Intent intent = new Intent().putExtra(MainActivity.PARAM_RESULT, factorial);
                    pi.send(WorkService.this, 0 , intent);
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
                stopSelf();
                return;
            }
            if (handler!=null){
                Bundle b = new Bundle();
                b.putLong("result",factorial);
                Message msg = new Message();
                msg.setData(b);
                handler.sendMessage(msg);
                stopSelf();
            }
        }
    }
}

