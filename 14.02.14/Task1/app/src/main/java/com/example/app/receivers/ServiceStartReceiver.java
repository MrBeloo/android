package com.example.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.app.Prefs;
import com.example.app.WorkService;

public class ServiceStartReceiver extends BroadcastReceiver {
    public ServiceStartReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(Prefs.LOG, "intent received");
        Log.d(Prefs.LOG, "" +intent.getIntExtra("value", 0));
        Intent serviceIntent = new Intent(context, WorkService.class);
        serviceIntent.putExtra("value", intent.getIntExtra("value", 0));
        context.startService(serviceIntent);
    }
}
