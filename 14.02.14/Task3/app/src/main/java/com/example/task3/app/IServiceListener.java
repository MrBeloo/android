package com.example.task3.app;

/**
 * Created by dev2 on 2/14/14.
 */
public interface IServiceListener {
    public void factorialCalculated(long factorial);
}
