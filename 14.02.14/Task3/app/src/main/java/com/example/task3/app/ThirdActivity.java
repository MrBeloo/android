package com.example.task3.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ThirdActivity extends Activity {

    Messenger mService = null;
    boolean mIsBound;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private ServiceConnection mConnection;

    TextView textView_result;
    EditText editText_input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_num);

        Log.d("beloo","third activity");

        textView_result = (TextView)findViewById(R.id.textView_result);
        editText_input = (EditText)findViewById(R.id.editText_input);
        Button bu_calc = (Button)findViewById(R.id.button_calc);
        bu_calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String value = editText_input.getText().toString();
                    if (!value.equals("")){
                        Message msg = Message.obtain(null, MessengerService.MSG_SET_VALUE,
                                Integer.parseInt(value), 0);
                        mService.send(msg);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                mService = new Messenger(service);

                try {
                    Message msg = Message.obtain(null,
                            MessengerService.MSG_REGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);

                } catch (RemoteException e) {
                    Log.d("beloo", "remote exception");
                }
            }

            public void onServiceDisconnected(ComponentName className) {
                mService = null;
            }
        };

    }

    @Override
    protected void onStart() {
        super.onStart();
        doBindService();
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        doUnbindService();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MessengerService.MSG_FACTORIAL_CALCULATED:
                    Log.d("beloo","third activity handled message");
                    Bundle data = msg.getData();
                    textView_result.setText(String.valueOf(data.getLong("value")));
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    void doBindService() {
        bindService(new Intent(this,
                MessengerService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            MessengerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                }
            }
            unbindService(mConnection);
            mIsBound = false;
        }
    }

}
