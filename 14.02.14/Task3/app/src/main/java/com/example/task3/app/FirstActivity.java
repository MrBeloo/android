package com.example.task3.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class FirstActivity extends Activity {

    Messenger mService = null;
    boolean mIsBound;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private ServiceConnection mConnection;

    Boolean firstCall = true;

    TextView textView_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        Log.d("beloo","fisrt activity");

        textView_result = (TextView)findViewById(R.id.textView_result);

        mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className,
                                           IBinder service) {
                mService = new Messenger(service);

                try {
                    Message msg = Message.obtain(null,
                            MessengerService.MSG_REGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);

                } catch (RemoteException e) {
                    Log.d("beloo", "remote exception");
                }
            }

            public void onServiceDisconnected(ComponentName className) {
                mService = null;
            }
        };

        if(firstCall){
            startActivity(new Intent(FirstActivity.this, SecondActivity.class));
            firstCall = false;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("call",firstCall);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        firstCall = savedInstanceState.getBoolean("call");
    }

    @Override
    protected void onStart() {
        super.onStart();
        doBindService();
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        doUnbindService();
        Log.d("beloo","First activity stopped");
    }

    class IncomingHandler extends PauseHandler {
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            switch (message.what) {
                case MessengerService.MSG_FACTORIAL_CALCULATED:
                    Log.d("beloo","first activity handled message");
                    Bundle data = message.getData();
                    textView_result.setText(String.valueOf(data.getLong("value")));
                    break;
            }
        }
    }

    void doBindService() {
        bindService(new Intent(this,
                MessengerService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            MessengerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                }
            }
            unbindService(mConnection);
            mIsBound = false;
        }
    }

}
