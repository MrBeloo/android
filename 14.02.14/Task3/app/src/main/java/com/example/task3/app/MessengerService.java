package com.example.task3.app;

import java.util.ArrayList;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;


public class MessengerService extends Service {

    NotificationManager mNM;
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();

    static final int MSG_REGISTER_CLIENT = 1;

    static final int MSG_UNREGISTER_CLIENT = 2;

    static final int MSG_SET_VALUE = 3;

    static final int MSG_FACTORIAL_CALCULATED = 4;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    Log.d("beloo", "service accepted value");
                    Integer inputNumber = msg.arg1;
                    (new CalcFactorial(inputNumber)).start();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    private class CalcFactorial extends Thread{

        private int inputNumber;

        public CalcFactorial(int inputNumber) {
            this.inputNumber = inputNumber;
        }

        @Override
        public void run() {
            long factorial = 1;
            for (long i=1; i<=inputNumber; i++){
                factorial*=i;
            }
            for (int i=mClients.size()-1; i>=0; i--) {
                try {
                    Message msg = new Message();
                    Bundle b = new Bundle();
                    b.putLong("value", factorial);
                    msg.setData(b);
                    msg.what = MSG_FACTORIAL_CALCULATED;
                    mClients.get(i).send(msg);
                    Log.d("beloo","factorial sended to clients");
                } catch (RemoteException e) {
                    mClients.remove(i);
                }
            }
        }
    }
}