package task2.app;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.*;
import android.provider.ContactsContract.*;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.VideoView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {

    public static final int CONTACT_ACTIVITY = 0;
    public static final int IMAGE_ACTIVITY = 1;
    public static final int VIDEO_ACTIVITY = 2;
    public static final int PHOTO_ACTIVITY = 3;

    private Bitmap bitmap;
    private ImageView imageView;
    private EditText phoneBox;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        phoneBox = (EditText)findViewById(R.id.editText_phone);
        imageView = (ImageView)findViewById(R.id.imageView_getted);
        videoView = (VideoView)findViewById(R.id.videoView_getted);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case CONTACT_ACTIVITY:
                    Cursor cursor = getContentResolver().query(data.getData(), null,
                            null, null, null);

                    if (cursor.moveToFirst()) {

                        String contactName = cursor.getString(cursor.getColumnIndexOrThrow(Contacts.DISPLAY_NAME));
                        phoneBox.setText(contactName);

                        //вылетает, печаль-тоска
                        //String contactId = cursor.getString(cursor.getColumnIndexOrThrow(Contacts.DISPLAY_NAME));

                        //Cursor phones = getContentResolver().query(Phone.CONTENT_URI, null,Phone.CONTACT_ID +" = "+ contactId,null, null);
//                        while (phones.moveToNext())
//                        {
//                            String phoneNumber = phones.getString(phones.getColumnIndex(Phone.NUMBER));
//                            phoneBox.setText(phoneNumber);
//                        }
//                        phones.close();
                    }

                break;

                case IMAGE_ACTIVITY:
                    try {
                        if (bitmap != null) {
                            bitmap.recycle();
                        }
                        InputStream stream = getContentResolver().openInputStream(data.getData());
                        bitmap = BitmapFactory.decodeStream(stream);
                        stream.close();
                        imageView.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case VIDEO_ACTIVITY:
                    videoView.setVideoPath(data.getData().toString());
                    videoView.start();
                    break;

                case PHOTO_ACTIVITY:
                    Bitmap bm = (Bitmap) data.getExtras().get("data");
                    imageView.setImageBitmap(bm);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onButtonClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.button_contact:
                intent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
                startActivityForResult(intent,CONTACT_ACTIVITY);
                break;
            case R.id.button_image:
                intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, IMAGE_ACTIVITY);
                break;
            case R.id.button_video:
                intent = new Intent(Intent.ACTION_PICK, null);
                intent.setType("video/*");
                startActivityForResult(intent, VIDEO_ACTIVITY);
                break;
            case R.id.button_photo:
                intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, PHOTO_ACTIVITY);
                break;

        }

    }

}
