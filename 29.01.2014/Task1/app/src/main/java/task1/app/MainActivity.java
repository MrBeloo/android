package task1.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onButtonClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.phone_button:
                intent = new Intent(Intent.ACTION_DIAL);
                startActivity(intent);
                break;
            case R.id.sms_button:
                Uri sms = Uri.parse("sms:");
                intent = new Intent(Intent.ACTION_VIEW,sms);
                startActivity(intent);
                break;
            case R.id.browser_button:
                Uri http = Uri.parse("https://bitbucket.org/MrBeloo/android/");
                intent = new Intent(Intent.ACTION_VIEW, http);
                startActivity(intent);
                break;
            case R.id.email_button:
                Uri mail = Uri.parse("mailto:");
                intent = new Intent(Intent.ACTION_VIEW, mail);
                startActivity(intent);
                break;
        }
    }

}
