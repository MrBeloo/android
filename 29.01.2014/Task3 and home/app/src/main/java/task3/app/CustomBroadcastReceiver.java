package task3.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by dev2 on 29.01.14.
 */
public class CustomBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("custom.intent.action.MANIFEST")) {
            Toast.makeText(context,"Broadcast received",Toast.LENGTH_SHORT).show();
        }
    }
}
