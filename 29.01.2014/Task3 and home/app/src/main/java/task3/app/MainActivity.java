package task3.app;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CodeBroadcastReceiver receiver = new CodeBroadcastReceiver();
        this.registerReceiver(receiver, new IntentFilter(
                "custom.intent.action.CODE"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public static Context getContext() {
        return getContext();
    }

    public void onClickSender(View view){
        Intent i;
        switch(view.getId()){
            case R.id.button_manifest:
                i = new Intent();
                i.setAction("custom.intent.action.MANIFEST");
                sendBroadcast(i);
                break;
            case R.id.button_code:
                i = new Intent();
                i.setAction("custom.intent.action.CODE");
                sendBroadcast(i);
                break;
        }
    }


}
