package com.example.app.Activity;

import android.app.Fragment;
import android.app.ListActivity;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;

import com.example.app.Fragments.ListBuffer;
import com.example.app.Fragments.TestListFragment;
import com.example.app.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecondActivity extends ListActivity implements ListBuffer {

    public static final String LOG = "beloo";

    private ArrayList<String> elems;

    private ListView listView;
    private GridView gridView;
    private Spinner spinner;
    private ExpandableListView expListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        elems = new ArrayList<String>(Arrays.asList(getString(R.string.elem1),getString(R.string.elem2),getString(R.string.elem3),getString(R.string.elem4)));

        listView = (ListView)findViewById(R.id.listView);
        gridView = (GridView)findViewById(R.id.gridView);
        spinner = (Spinner)findViewById(R.id.spinner);
        expListView = (ExpandableListView)findViewById(R.id.expListView);
        gridView.setNumColumns(2);

        ExpandableListViewController controller = new ExpandableListViewController();
        controller.AddGroup(getString(R.string.group1), elems.subList(0,2));
        controller.AddGroup(getString(R.string.group2), elems.subList(2,4));

        ListType param = null;
        Bundle b = getIntent().getExtras();
        if (b != null) {
            Log.d(LOG, "bundle accepted");
            param = (ListType)b.getSerializable("param");
        } else Log.d(LOG, "bundle accept error");

        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elems);


        Log.d(LOG, param.toString());
        switch (param){
            case ListView:
                listView.setAdapter(ad);
                listView.setVisibility(View.VISIBLE);
                break;
            case GridView:
                gridView.setAdapter(ad);
                gridView.setVisibility(View.VISIBLE);
                break;
            case ListActivity:
                setListAdapter(ad);
                break;
            case ListFragment:
                Fragment fr = new TestListFragment();
                getFragmentManager().beginTransaction()
                        .add(R.id.container,fr)
                        .commit();
                break;
            case ExpandableListView:
                expListView.setAdapter(controller.GetAdapter());
                expListView.setVisibility(View.VISIBLE);
                break;
            case Spinner:
                spinner.setAdapter(ad);
                spinner.setVisibility(View.VISIBLE);
                break;
        }
    }

    private class ExpandableListViewController{
        private ArrayList<Map<String, String>> groupData;
        private ArrayList<Map<String, String>> childDataItem;
        private ArrayList<ArrayList<Map<String, String>>> childData;
        private Map<String, String> m;
        private String groupFrom[];
        private int groupTo[];
        private String childFrom[];
        private int childTo[];

        public ExpandableListViewController() {
            groupData = new ArrayList<Map<String, String>>();
            groupFrom = new String[] {"groupName"};
            groupTo = new int[] {android.R.id.text1};
            childFrom = new String[] {"elem"};
            childTo = new int[] {android.R.id.text1};
            childData = new ArrayList<ArrayList<Map<String, String>>>();
        }

        public void AddGroup(String groupName, List<String> elems){
            m = new HashMap<String, String>();
            m.put("groupName", groupName);
            groupData.add(m);

            childDataItem = new ArrayList<Map<String, String>>();
            for (String elem : elems) {
                m = new HashMap<String, String>();
                m.put("elem", elem);
                childDataItem.add(m);
            }
            childData.add(childDataItem);
        }

        public SimpleExpandableListAdapter GetAdapter (){
            return new SimpleExpandableListAdapter(
                    SecondActivity.this,
                    groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    groupFrom,
                    groupTo,
                    childData,
                    android.R.layout.simple_list_item_1,
                    childFrom,
                    childTo);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public ArrayList<String> GetListElems() {
        return elems;
    }
}
