package com.example.app.Activity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.example.app.R;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    ArrayList<Button> buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttons = new ArrayList<Button>();

        buttons.add((Button)findViewById(R.id.button1));
        buttons.add((Button)findViewById(R.id.button2));
        buttons.add((Button)findViewById(R.id.button3));
        buttons.add((Button)findViewById(R.id.button4));
        buttons.add((Button)findViewById(R.id.button5));
        buttons.add((Button)findViewById(R.id.button6));

        for (Button bu : buttons) {
            bu.setOnClickListener(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public void onClick(View view) {
        Intent i = new Intent(this, SecondActivity.class);
        Bundle b = new Bundle();
        switch (view.getId()){
            case R.id.button1:
                b.putSerializable("param", ListType.ListView);
                break;
            case R.id.button2:
                b.putSerializable("param", ListType.GridView);
                break;
            case R.id.button3:
                b.putSerializable("param", ListType.Spinner);
                break;
            case R.id.button4:
                b.putSerializable("param", ListType.ListActivity);
                break;
            case R.id.button5:
                b.putSerializable("param", ListType.ListFragment);
                break;
            case R.id.button6:
                b.putSerializable("param", ListType.ExpandableListView);
                break;
        }
        i.putExtras(b);
        startActivity(i);
    }
}
