package com.example.app.Activity;

public enum ListType{
    ListView,
    GridView,
    Spinner,
    ListActivity,
    ListFragment,
    ExpandableListView
}
