package com.example.app.Adapters;

import java.util.ArrayList;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app.R;

import java.util.ArrayList;

/**
 * Created by dev2 on 03.02.14.
 */
public class CustomListAdapter extends BaseAdapter{

    class ViewHolder {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        TextView textView4;
        TextView textView5;
        TextView textView6;
    }


    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<String> someObjs;

    public CustomListAdapter(Context context, ArrayList<String> elems) {
        ctx = context;
        someObjs = elems;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return someObjs.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null){

            Log.d("beloo", "null converView");
            convertView = lInflater.inflate(R.layout.listitem, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.textView1 = ((TextView) convertView.findViewById(R.id.textView1));
            viewHolder.textView2 = ((TextView) convertView.findViewById(R.id.textView2));
            viewHolder.textView3 = ((TextView) convertView.findViewById(R.id.textView3));
            viewHolder.textView4 = ((TextView) convertView.findViewById(R.id.textView4));
            viewHolder.textView5 = ((TextView) convertView.findViewById(R.id.textView5));
            viewHolder.textView6 = ((TextView) convertView.findViewById(R.id.textView6));

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
            Log.d("beloo", "else," + viewHolder);
        }

        viewHolder.textView1.setText(someObjs.get(position));
        viewHolder.textView2.setText(someObjs.get(position));
        viewHolder.textView3.setText(someObjs.get(position));
        viewHolder.textView4.setText(someObjs.get(position));
        viewHolder.textView5.setText(someObjs.get(position));
        viewHolder.textView6.setText(someObjs.get(position));

        return convertView;
    }


    @Override
    public Object getItem(int i) {
        return someObjs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}

