package com.example.app.Activity;

import android.app.Fragment;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;

import com.example.app.Adapters.CustomExpandedAdapter;
import com.example.app.Adapters.CustomGridAdapter;
import com.example.app.Adapters.CustomListAdapter;
import com.example.app.Adapters.CustomSpinnerAdapter;
import com.example.app.Fragments.ListBuffer;
import com.example.app.Fragments.TestListFragment;
import com.example.app.R;
import com.example.app.ViewModels.ArticleModel;
import com.example.app.ViewModels.ImageModel;
import com.example.app.ViewModels.SeriesMovieModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecondActivity extends ListActivity implements ListBuffer {

    public static final String LOG = "beloo";

    private ArrayList<String> elems;
    private ArrayList<String> some;
    private ArrayList<ImageModel> images;
    private ArrayList<SeriesMovieModel> series;
    private ArrayList<ArticleModel> articles;

    private ListView listView;
    private GridView gridView;
    private Spinner spinner;
    private ExpandableListView expListView;


    private OnClickController controller;

    private class OnClickController implements OnCLickSpinnerListener{
        @Override
        public void OnClick(int position) {
            Log.d("beloo", "spinner select");
            spinner.setSelection(position);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        /// Какие-то данные из бд
        elems = new ArrayList<String>(Arrays.asList(getString(R.string.elem1),getString(R.string.elem2),getString(R.string.elem3),getString(R.string.elem4)));
        some = new ArrayList<String>(Arrays.asList(getString(R.string.something),getString(R.string.something),getString(R.string.something)));
        images = new ArrayList<ImageModel>(Arrays.asList(
            new ImageModel(R.drawable.ic_launcher, getString(R.string.image1)), new ImageModel(R.drawable.ic_launcher, getString(R.string.image2)),
            new ImageModel(R.drawable.ic_launcher, getString(R.string.image3)), new ImageModel(R.drawable.ic_launcher, getString(R.string.image4))
        ));
        series = new ArrayList<SeriesMovieModel>(Arrays.asList(
                new SeriesMovieModel(getString(R.string.series1),525, 4.2f), new SeriesMovieModel(getString(R.string.series2),600, 4.9f),
                new SeriesMovieModel(getString(R.string.series3),556, 3.8f)
        ));
        articles = new ArrayList<ArticleModel>(Arrays.asList(
                new ArticleModel(R.drawable.ic_launcher,"Майдан наступает",getString(R.string.someText)),
                new ArticleModel(R.drawable.ic_launcher,"Майдан отступает",getString(R.string.someText)),
                new ArticleModel(R.drawable.ic_launcher,"Майдан наступает",getString(R.string.someText)),
                new ArticleModel(R.drawable.ic_launcher,"Фюле приехал",getString(R.string.someText)),
                new ArticleModel(R.drawable.ic_launcher,"Фюле уехал",getString(R.string.someText))
        ));

        listView = (ListView)findViewById(R.id.listView);
        gridView = (GridView)findViewById(R.id.gridView);
        spinner = (Spinner)findViewById(R.id.spinner_1);
        expListView = (ExpandableListView)findViewById(R.id.expListView);
        gridView.setNumColumns(2);

        ExpandableListViewController<ArticleModel> controller = new ExpandableListViewController<ArticleModel>();
        controller.AddGroup(getString(R.string.group1), articles.subList(0, 3));
        controller.AddGroup(getString(R.string.group2), articles.subList(3,5));

        ListType param = null;
        Bundle b = getIntent().getExtras();
        if (b != null) {
            Log.d(LOG, "bundle accepted");
            param = (ListType)b.getSerializable("param");
        } else Log.d(LOG, "bundle accept error");

        ArrayAdapter<String> ad = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elems);
        CustomListAdapter ca = new CustomListAdapter(this, some);
        CustomGridAdapter cg = new CustomGridAdapter(this,images);
        OnCLickSpinnerListener sl = new OnClickController();
        CustomSpinnerAdapter cs = new CustomSpinnerAdapter(this, R.layout.spinneritem, sl, series);

        Log.d(LOG, param.toString());
        switch (param){
            case ListView:
                listView.setAdapter(ca);
                listView.setVisibility(View.VISIBLE);
                break;
            case GridView:
                gridView.setAdapter(cg);
                gridView.setVisibility(View.VISIBLE);
                break;
            case ListActivity:
                setListAdapter(ad);
                break;
            case ListFragment:
                Fragment fr = new TestListFragment();
                getFragmentManager().beginTransaction()
                        .add(R.id.container,fr)
                        .commit();
                break;
            case ExpandableListView:
                expListView.setAdapter(controller.GetAdapter());
                expListView.setVisibility(View.VISIBLE);
                break;
            case Spinner:
                spinner.setAdapter(cs);
                spinner.setVisibility(View.VISIBLE);
                break;
        }
    }

    private class ExpandableListViewController<T>{
        private ArrayList<Map<String, String>> groupData;
        private ArrayList<Map<String, T>> childDataItem;
        private ArrayList<ArrayList<Map<String, T>>> childData;
        private Map<String, T> m;
        private Map<String, String> ms;
        private String groupFrom[];
        private int groupTo[];
        private String childFrom[];
        private int childTo[];

        public ExpandableListViewController() {
            groupData = new ArrayList<Map<String, String>>();
            groupFrom = new String[] {"groupName"};
            groupTo = new int[] {android.R.id.text1};
            childFrom = new String[] {"elem"};
            childTo = new int[] {android.R.id.text1};
            childData = new ArrayList<ArrayList<Map<String, T>>>();
        }

        public void AddGroup(String groupName, List<T> elems){
            ms = new HashMap<String, String>();
            ms.put("groupName", groupName);
            groupData.add(ms);

            childDataItem = new ArrayList<Map<String, T>>();
            for (T elem : elems) {
                m = new HashMap<String, T>();
                m.put("elem", elem);
                childDataItem.add(m);
            }
            childData.add(childDataItem);
        }

        public CustomExpandedAdapter GetAdapter (){
            return new CustomExpandedAdapter(
                    SecondActivity.this,
                    groupData,
                    android.R.layout.simple_expandable_list_item_1,
                    groupFrom,
                    groupTo,
                    childData,
                    android.R.layout.simple_list_item_1,
                    childFrom,
                    childTo);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public ArrayList<String> GetListElems() {
        return elems;
    }
}
