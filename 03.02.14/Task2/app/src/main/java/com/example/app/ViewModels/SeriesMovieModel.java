package com.example.app.ViewModels;

/**
 * Created by dev2 on 03.02.14.
 */
public class SeriesMovieModel {
    public String title;
    public Integer sizeMb;
    public float rating;

    public SeriesMovieModel(String title, int sizeMb, float rating) {
        this.title = title;
        this.sizeMb = sizeMb;
        this.rating = rating;
    }
}
