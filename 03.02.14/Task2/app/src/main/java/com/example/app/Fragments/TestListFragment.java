package com.example.app.Fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.app.Activity.SecondActivity;

/**
 * Created by dev2 on 03.02.14.
 */
public class TestListFragment extends ListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d(SecondActivity.LOG, getActivity().toString());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, ((ListBuffer)getActivity()).GetListElems());
        Log.d(SecondActivity.LOG, ((ListBuffer) (getActivity())).GetListElems().toString());
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
