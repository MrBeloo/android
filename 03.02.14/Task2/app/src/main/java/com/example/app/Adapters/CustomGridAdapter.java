package com.example.app.Adapters;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.ViewModels.ImageModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by dev2 on 03.02.14.
 */
public class CustomGridAdapter extends BaseAdapter{

    class ViewHolder{
        ImageView imageView;
        TextView imageTitle;
    }

    private Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<ImageModel> someObjs;

    public CustomGridAdapter(Context context, ArrayList<ImageModel> elems) {
        ctx = context;
        someObjs = elems;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return someObjs.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.griditem, parent, false);

            viewHolder.imageTitle = (TextView)convertView.findViewById(R.id.textView_ititle);
            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageView1);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.imageView.setImageResource(someObjs.get(position).imageResource);
        viewHolder.imageTitle.setText(someObjs.get(position).title);

        return convertView;
    }


    @Override
    public Object getItem(int i) {
        return someObjs.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}

