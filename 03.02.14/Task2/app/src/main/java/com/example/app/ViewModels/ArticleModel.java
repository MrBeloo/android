package com.example.app.ViewModels;

/**
 * Created by dev2 on 03.02.14.
 */
public class ArticleModel {
    public int articleImage;
    public String title;
    public String preview;

    public ArticleModel(int articleImage, String title, String preview) {
        this.articleImage = articleImage;
        this.title = title;
        this.preview = preview;
    }
}
