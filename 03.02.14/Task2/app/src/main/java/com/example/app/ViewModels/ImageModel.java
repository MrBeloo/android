package com.example.app.ViewModels;

/**
 * Created by dev2 on 03.02.14.
 */
public class ImageModel {
    public int imageResource;
    public String title;

    public ImageModel(int imageResource, String title){
        this.imageResource = imageResource;
        this.title = title;
    }
}
