package com.example.app.Adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.ViewModels.ArticleModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dev2 on 03.02.14.
 */
public class CustomExpandedAdapter extends SimpleExpandableListAdapter {

    class ViewHolder{
        TextView articleTitle;
        EditText preview;
        ImageView imageArticle;
    }

    private Context ctx;
    private LayoutInflater lInflater;

    public CustomExpandedAdapter(Context context, List<? extends Map<String, ?>> groupData, int groupLayout, String[] groupFrom, int[] groupTo, List<? extends List<? extends Map<String, ?>>> childData, int childLayout, String[] childFrom, int[] childTo) {
        super(context, groupData, groupLayout, groupFrom, groupTo, childData, childLayout, childFrom, childTo);
        ctx = context;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.expandedchild, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.articleTitle = (TextView)convertView.findViewById(R.id.textView_titleArticle);
            viewHolder.imageArticle = (ImageView)convertView.findViewById(R.id.imageView_article);
            viewHolder.preview = (EditText)convertView.findViewById(R.id.editText_preview);

            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Map<String, ArticleModel> childDataItem = (Map<String, ArticleModel>)getChild(groupPosition, childPosition);
        ArticleModel aModel = childDataItem.get("elem");

        viewHolder.articleTitle.setText(aModel.title);
        viewHolder.preview.setText(aModel.preview);
        viewHolder.imageArticle.setImageResource(aModel.articleImage);

        return convertView;
    }
}
