package com.example.app.Adapters;

import java.text.MessageFormat;
import java.util.ArrayList;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.app.Activity.OnCLickSpinnerListener;
import com.example.app.R;
import com.example.app.ViewModels.SeriesMovieModel;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * Created by dev2 on 03.02.14.
 */
public class CustomSpinnerAdapter extends ArrayAdapter{

    class ViewHolder {
        TextView title;
        TextView size;
        RatingBar seriesRating;
        RelativeLayout relativeSpinner;
    }

    Context ctx;
    private LayoutInflater lInflater;
    private ArrayList<SeriesMovieModel> someObjs;
    private int layoutId;
    OnCLickSpinnerListener listener;

    public CustomSpinnerAdapter(Context context, int layoutId, OnCLickSpinnerListener listener, ArrayList<SeriesMovieModel> elems) {
        super(context, layoutId, elems);
        ctx = context;
        this.listener = listener;
        this.layoutId = layoutId;
        someObjs = elems;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = getCustomView(position, convertView, parent);
        return view;
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        Log.d("beloo", "before");
        if (convertView == null){
            Log.d("beloo", "null converView");

            viewHolder = new ViewHolder();
            convertView = lInflater.inflate(layoutId, parent, false);

            viewHolder.relativeSpinner = (RelativeLayout)convertView.findViewById(R.id.relative_spinner);
            viewHolder.title = (TextView) convertView.findViewById(R.id.textView_title);
            viewHolder.size = (TextView) convertView.findViewById(R.id.textView_size);
            viewHolder.seriesRating = (RatingBar) convertView.findViewById(R.id.ratingBar1);

            convertView.setTag(viewHolder);
        } else {

            Log.d("beloo", "hereis");
            viewHolder = (ViewHolder)convertView.getTag();
            Log.d("beloo", "else," + viewHolder);
        }

        viewHolder.relativeSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnClick(position);
            }
        });
        viewHolder.seriesRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnClick(position);
            }
        });
        viewHolder.size.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnClick(position);
            }
        });

        viewHolder.title.setText(someObjs.get(position).title);
        viewHolder.size.setText(
                MessageFormat.format("{0}: {1} {2}", ctx.getString(R.string.size), someObjs.get(position).sizeMb.toString(), ctx.getString(R.string.mb)));
        viewHolder.seriesRating.setRating(someObjs.get(position).rating);

        return convertView;
    }
}

