package com.example.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class DescriptionsFragment extends Fragment {

    View staticView;

    public DescriptionsFragment(View staticView) {
        this.staticView = staticView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return staticView;
    }
}
