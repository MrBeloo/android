package com.example.app.Models;

import java.io.Serializable;

/**
 * Created by dev2 on 04/02/14.
 */
public class OrderModel implements Serializable{
    public Integer id;
    public int productId;
    public int vendorId;
    public int customerId;

    public static String ProductId = "productId";
    public static String VendorId = "vendorId";
    public static String CustomerId = "customerId";
    public static String Id = "_id";
    public static String Bundle = "mOrder";

    @Override
    public String toString() {
        return "Id = " + id +" productId = " + productId + " vendorId = " + vendorId + " customerId = "+ customerId;
    }
}
