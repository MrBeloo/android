package com.example.app.ViewModels;

import android.view.View;
import android.widget.TextView;

import com.example.app.Models.VendorModel;
import com.example.app.R;

/**
 * Created by Beloo on 09.02.14.
 */
public class VendorItemVM <T extends TextView> {

    public View view;
    public TextView name;
    public TextView id;

    public VendorItemVM(View view){
        this.view = view;
        id = (TextView)view.findViewById(R.id.textView_id);
        name = (TextView)view.findViewById(R.id.textView_name);
    }

    public boolean IsEmpty(){
        return name.getText().toString().matches("");
    }

    public VendorModel GetCustomerModel(){
        VendorModel mVendor = new VendorModel();
        mVendor.id = Integer.parseInt(id.getText().toString());
        mVendor.name = name.getText().toString();
        return mVendor;
    }
}