package com.example.app.ViewModels;

import android.view.View;
import android.widget.TextView;

import com.example.app.R;

/**
 * Created by Beloo on 09.02.14.
 */
public class OrderItemViewModel {

    public View view;
    public TextView productName;
    public TextView id;
    public TextView vendorName;
    public TextView customerName;

    public OrderItemViewModel(View view){
        this.view = view;
        id = (TextView)view.findViewById(R.id.textView_id);
        productName = (TextView)view.findViewById(R.id.textView_productName);
        vendorName = (TextView)view.findViewById(R.id.textView_vendorName);
        customerName = (TextView)view.findViewById(R.id.textView_customerName);
    }

}
