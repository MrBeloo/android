package com.example.app.Models;

import java.io.Serializable;

/**
 * Created by dev2 on 04/02/14.
 */
public class VendorModel implements Serializable{
    public Integer id;
    public String name;

    public static String Name = "name";
    public static String Id = "_id";
    public static String Bundle = "mVendor";
}
