package com.example.app.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.Adapters.ProductListCursorAdapter;
import com.example.app.DbProcessors.ProductProcessor;
import com.example.app.Fragments.dialog.IUpdateProductListener;
import com.example.app.Fragments.dialog.UpdateProductFragment;
import com.example.app.Models.ProductModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.ProductItemVM;

/**
 * Created by dev2 on 04/02/14.
 */
public class ProductsFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>, IUpdateProductListener {

    private static final int DATABASE_LOADER = 2;

    private ProductListCursorAdapter productAdapter;
    private Context context;
    private Button bu_Update;
    private Button bu_Add;
    private ListView productsListView;

    private Boolean addButtonNewState;
    private ProductItemVM<EditText> addRowView;
    private ProductItemVM<TextView> selectedView;

    private ProductProcessor prodProc;
    private LayoutInflater inflater;
    private ViewGroup viewGroup;
    private LinearLayout linear;

    @Override
    public void UpdateClicked(ProductModel mProduct) {
        Log.d(Prefs.LOG,"before database update");
        prodProc.UpdateProductById(mProduct);
        getFragmentManager().popBackStack();
        Toast.makeText(context, getResources().getString(R.string.haveUpdated), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        addButtonNewState = true;
        this.inflater = inflater;
        context = getActivity();
        View view = inflater.inflate(R.layout.database_fragment, null);
        prodProc = ProductProcessor.getInstance();
        viewGroup = container;

        Log.d(Prefs.LOG,"product fragment view");

        bu_Add = (Button)view.findViewById(R.id.button_add);
        Button bu_Delete = (Button)view.findViewById(R.id.button_delete);
        bu_Update = (Button)view.findViewById(R.id.button_update);
        productsListView = (ListView)view.findViewById(R.id.listView_elements);
        linear = (LinearLayout)view.findViewById(R.id.main_linear);

        ProductItemVM header = new ProductItemVM<TextView>(inflater.inflate(R.layout.product_db_view, container, false));
        header.id.setText(ProductModel.Id);
        header.productName.setText(ProductModel.ProductName);
        header.manufacturer.setText(ProductModel.Manufacturer);
        header.price.setText(ProductModel.Price);

        getFragmentManager().beginTransaction().add(R.id.description_layout, new DescriptionsFragment(header.view)).commit();

        Log.d(Prefs.LOG, "before create cursor");

        productAdapter = new ProductListCursorAdapter(context, null, true);
        addRowView = new ProductItemVM<EditText>(inflater.inflate(R.layout.product_db_edit, null, false));
        addRowView.view.setVisibility(View.INVISIBLE);
        productsListView.addFooterView(addRowView.view);
        productsListView.setAdapter(productAdapter);

        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(Prefs.LOG, "clicked");
                selectedView = new ProductItemVM<TextView>(view);
                view.setSelected(true);
            }
        });
        bu_Add.setOnClickListener(this);
        bu_Delete.setOnClickListener(this);
        bu_Update.setOnClickListener(this);

        getLoaderManager().initLoader(DATABASE_LOADER, null, this).forceLoad();


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_add:
                if (addButtonNewState){
                    addButtonNewState = !addButtonNewState;
                    bu_Add.setText(getResources().getString(R.string.add));
                    addRowView.id.setText(String.valueOf(prodProc.GetLastId() + 1));
                    addRowView.view.setVisibility(View.VISIBLE);
                    productsListView.setSelection(productAdapter.getCount());
                } else {
                    if (addRowView.IsEmpty()){
                        Toast.makeText(context, context.getResources().getString(R.string.needsFillFields), Toast.LENGTH_SHORT).show();
                    } else {
                        addRowView.view.setVisibility(View.INVISIBLE);
                        addButtonNewState = !addButtonNewState;
                        bu_Add.setText(getResources().getString(R.string.newbutton));

                        //добавляем в базу
                        prodProc.CreateProduct(addRowView.GetProductModel());

                        Toast.makeText(context, context.getResources().getString(R.string.haveAdded), Toast.LENGTH_SHORT).show();
                        getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    }
                }

                break;
            case R.id.button_delete:
                if (selectedView == null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    prodProc.DeleteProductById(Integer.parseInt(selectedView.id.getText().toString()));
                    Toast.makeText(context, context.getResources().getString(R.string.haveDeleted), Toast.LENGTH_SHORT).show();
                    getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    selectedView = null;
                }

                break;
            case R.id.button_update:
                if (selectedView==null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    Bundle b= new Bundle();
                    b.putSerializable(ProductModel.Bundle, selectedView.GetProductModel());
                    UpdateProductFragment uf = new UpdateProductFragment(this);
                    uf.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, uf)
                            .addToBackStack(null).commit();
                }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.d(Prefs.LOG, "on create loader");
        Loader<Cursor> cursor = null;
        switch (i){
            case DATABASE_LOADER:
                cursor = new ProductProcessor.CursorReader(getActivity());
                break;
        }
        return cursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        productAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) { }
}
