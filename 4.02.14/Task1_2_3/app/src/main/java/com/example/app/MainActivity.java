
package com.example.app;

import com.example.app.DbProcessors.*;
import com.example.app.Fragments.ButtonsFragment;
import com.example.app.Fragments.CustomersFragment;
import com.example.app.Fragments.OrdersFragment;
import com.example.app.Fragments.ProductsFragment;
import com.example.app.Fragments.VendorsFragment;

import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private SQLiteDatabase db;

    public static final String LOG = "beloo";

    //здесь нужны сервисы, но будут процессоры
    private ProductProcessor prodProc;
    private OrderProcessor ordProc;
    private CustomerProcessor custProc;
    private VendorProcessor vendProc;

    //fragments of this activity
    private ButtonsFragment bf;
    private ProductsFragment pf;
    private OrdersFragment of;
    private CustomersFragment cf;
    private VendorsFragment vf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        DatabaseHelper.context = this;
        //db = dbHelper.getWritableDatabase();

        prodProc = ProductProcessor.getInstance();
        custProc = CustomerProcessor.getInstance();
        vendProc = VendorProcessor.getInstance();

        bf = new ButtonsFragment(this);
        pf = new ProductsFragment();
        of = new OrdersFragment();
        cf = new CustomersFragment();
        vf = new VendorsFragment();

        if (savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, bf).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        Log.d("beloo","onclick");
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.button_order:
                trans.replace(R.id.container, of);
                break;
            case R.id.button_customers:
                trans.replace(R.id.container, cf);
                break;
            case R.id.button_vendors:
                trans.replace(R.id.container, vf);
                break;
            case R.id.button_prod:
                Log.d(Prefs.LOG,"button prod clicked");
                trans.replace(R.id.container, pf);
                break;
        }
        trans.addToBackStack(null).commit();
    }
}
