package com.example.app.Fragments.dialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.Models.CustomerModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.CustomerItemVM;

public class UpdateCustomerFragment extends Fragment {

    IUpdateCustomerListener iListener;
    CustomerModel mCustomer;
    CustomerItemVM customerView;

    public UpdateCustomerFragment(IUpdateCustomerListener iListener) {
        this.iListener = iListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.customer_update, container, false);
        customerView = new CustomerItemVM(v);
        Button bu_update = (Button)v.findViewById(R.id.button_update);

        Bundle b = getArguments();
        mCustomer = (CustomerModel)b.getSerializable(CustomerModel.Bundle);
        customerView.id.setText(mCustomer.id.toString());
        customerView.name.setText(mCustomer.name);
        customerView.phone.setText(mCustomer.phone.toString());
        customerView.address.setText(mCustomer.address);

        bu_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            iListener.UpdateClicked(customerView.GetCustomerModel());
            }
        });

        return v;
    }
}
