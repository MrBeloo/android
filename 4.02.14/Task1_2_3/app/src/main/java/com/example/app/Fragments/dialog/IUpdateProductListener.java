package com.example.app.Fragments.dialog;

import com.example.app.Models.ProductModel;

/**
 * Created by dev2 on 2/6/14.
 */

public interface IUpdateProductListener {
    public void UpdateClicked(ProductModel mProduct);
}
