package com.example.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.app.MainActivity;
import com.example.app.R;

public class ButtonsFragment extends Fragment implements View.OnClickListener{

    private View.OnClickListener listener;

    public ButtonsFragment(View.OnClickListener listener) {
        this.listener = listener;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.buttons_fragment, null);

        Button orders = (Button) (v.findViewById(R.id.button_order));
        Button customers = (Button) (v.findViewById(R.id.button_customers));
        Button products = (Button) (v.findViewById(R.id.button_prod));
        Button vendors = (Button) (v.findViewById(R.id.button_vendors));

        products.setOnClickListener(this);
        orders.setOnClickListener(this);
        vendors.setOnClickListener(this);
        customers.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        listener.onClick(view);
    }
}
