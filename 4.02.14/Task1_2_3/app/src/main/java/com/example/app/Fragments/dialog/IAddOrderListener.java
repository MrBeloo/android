package com.example.app.Fragments.dialog;

import com.example.app.Models.OrderModel;

/**
 * Created by dev2 on 2/7/14.
 */
public interface IAddOrderListener extends IOrderListener{
    @Override
    public void ProcessOrder(OrderModel mOrder);
}
