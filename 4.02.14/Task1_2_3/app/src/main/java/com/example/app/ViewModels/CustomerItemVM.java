package com.example.app.ViewModels;

import android.view.View;
import android.widget.TextView;

import com.example.app.Models.CustomerModel;
import com.example.app.R;

/**
 * Created by Beloo on 09.02.14.
 */
public class CustomerItemVM<T extends TextView> {

    public View view;
    public TextView name;
    public TextView phone;
    public TextView address;
    public TextView id;

    public CustomerItemVM(View view) {
        this.view = view;
        name = (T)view.findViewById(R.id.textView_name);
        phone = (T)view.findViewById(R.id.textView_phone);
        address = (T)view.findViewById(R.id.textView_address);
        id = (T)view.findViewById(R.id.textView_id);
    }

    public boolean IsEmpty(){
        return name.getText().toString().matches("") ||
                phone.getText().toString().matches("") || address.getText().toString().matches("");
    }

    public CustomerModel GetCustomerModel(){
        CustomerModel mCustomer = new CustomerModel();
        mCustomer.id = Integer.parseInt(id.getText().toString());
        mCustomer.name = name.getText().toString();
        mCustomer.phone = phone.getText().toString();
        mCustomer.address = address.getText().toString();
        return mCustomer;
    }

}
