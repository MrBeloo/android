package com.example.app.Fragments.dialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.Models.ProductModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.ProductItemVM;

public class UpdateProductFragment extends Fragment {


    IUpdateProductListener iListener;
    ProductModel mProduct;

    ProductItemVM<EditText> productView;

    public UpdateProductFragment(IUpdateProductListener iListener) {
        this.iListener = iListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.product_update, container, false);

        productView = new ProductItemVM<EditText>(v);
        Button bu_update = (Button)v.findViewById(R.id.button_update);

        Bundle b = getArguments();
        mProduct = (ProductModel)b.getSerializable(ProductModel.Bundle);
        productView.id.setText(mProduct.id.toString());
        productView.productName.setText(mProduct.productName);
        productView.price.setText(mProduct.price.toString());
        productView.manufacturer.setText(mProduct.manufacturer);

        bu_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iListener.UpdateClicked(productView.GetProductModel());
            }
        });

        return v;
    }
}
