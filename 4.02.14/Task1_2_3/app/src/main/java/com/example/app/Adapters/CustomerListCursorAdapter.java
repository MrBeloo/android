package com.example.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.app.Models.CustomerModel;
import com.example.app.R;
import com.example.app.ViewModels.CustomerItemVM;

/**
 * Created by dev2 on 2/7/14.
 */
public class CustomerListCursorAdapter extends CursorAdapter {
    public CustomerListCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        lInflater = (LayoutInflater.from(context));
    }

    LayoutInflater lInflater;

    public int getPosition(int cursorId){
        for (int i=0; i< getCount(); i++){
            if (getItemId(i) == cursorId){
                return i;
            }
        }
        return -1;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {}

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        CustomerItemVM<TextView> viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.customer_db_view, parent, false);
            viewHolder = new CustomerItemVM<TextView>(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CustomerItemVM<TextView>)convertView.getTag();
        }

        Cursor cursor = (Cursor)getItem(position);

        viewHolder.id.setText(cursor.getString(cursor.getColumnIndex(CustomerModel.Id)));
        viewHolder.name.setText(cursor.getString(cursor.getColumnIndex(CustomerModel.Name)));
        viewHolder.phone.setText(cursor.getString(cursor.getColumnIndex(CustomerModel.Phone)));
        viewHolder.address.setText(cursor.getString(cursor.getColumnIndex(CustomerModel.Address)));

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return lInflater.inflate(R.layout.customer_db_view, viewGroup,false);
    }
}
