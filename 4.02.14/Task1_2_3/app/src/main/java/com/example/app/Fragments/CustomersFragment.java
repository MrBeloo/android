package com.example.app.Fragments;


import android.content.Context;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.Adapters.CustomerListCursorAdapter;
import com.example.app.DbProcessors.CustomerProcessor;
import com.example.app.Fragments.dialog.IUpdateCustomerListener;
import com.example.app.Fragments.dialog.UpdateCustomerFragment;
import com.example.app.Models.CustomerModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.CustomerItemVM;

/**
 * Created by dev2 on 04/02/14.
 */
public class CustomersFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>, IUpdateCustomerListener {

    private static final int DATABASE_LOADER = 3;

    private CustomerListCursorAdapter customerAdapter;
    private Context context;
    private Button bu_Add;
    private ListView customersListView;
    private LayoutInflater inflater;

    private Boolean addButtonNewState;
    private CustomerItemVM<EditText> newRowView;
    private CustomerItemVM<TextView> selectedView;

    private CustomerProcessor customerProcessor;

    @Override
    public void UpdateClicked(CustomerModel mCustomer) {
        Log.d(Prefs.LOG,"before database update");
        customerProcessor.UpdateCustomerById(mCustomer);
        getFragmentManager().popBackStack();
        Toast.makeText(context, getResources().getString(R.string.haveUpdated), Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        addButtonNewState = true;
        this.inflater = inflater;
        context = getActivity();
        View view = inflater.inflate(R.layout.database_fragment, null);
        customerProcessor = CustomerProcessor.getInstance();

        Log.d(Prefs.LOG,"customer fragment view");

        bu_Add = (Button)view.findViewById(R.id.button_add);
        Button bu_Delete = (Button)view.findViewById(R.id.button_delete);
        Button bu_Update = (Button) view.findViewById(R.id.button_update);
        customersListView = (ListView)view.findViewById(R.id.listView_elements);
        CustomerItemVM header = new CustomerItemVM(inflater.inflate(R.layout.customer_db_view, container, false));
        header.id.setText(CustomerModel.Id);
        header.name.setText(CustomerModel.Name);
        header.address.setText(CustomerModel.Address);
        header.phone.setText(CustomerModel.Phone);

        getFragmentManager().beginTransaction()
                .add(R.id.description_layout, new DescriptionsFragment(header.view)).commit();

        Log.d(Prefs.LOG, "before create cursor");

        customerAdapter = new CustomerListCursorAdapter(context, null, true);
        newRowView = new CustomerItemVM(inflater.inflate(R.layout.customer_db_edit, null, false));
        newRowView.view.setVisibility(View.INVISIBLE);
        customersListView.addFooterView(newRowView.view);
        customersListView.setAdapter(customerAdapter);

        customersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(Prefs.LOG, "clicked");
                selectedView = new CustomerItemVM<TextView>(view);
                view.setSelected(true);
            }
        });
        bu_Add.setOnClickListener(this);
        bu_Delete.setOnClickListener(this);
        bu_Update.setOnClickListener(this);

        getLoaderManager().initLoader(DATABASE_LOADER, null, this).forceLoad();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_add:
                if (addButtonNewState){
                    addButtonNewState = !addButtonNewState;
                    bu_Add.setText(getResources().getString(R.string.add));
                    newRowView.id.setText(String.valueOf(customerProcessor.GetLastId() + 1));
                    newRowView.view.setVisibility(View.VISIBLE);
                    customersListView.setSelection(customerAdapter.getCount() - 1);
                } else {
                    if (newRowView.IsEmpty()){
                        Toast.makeText(context, context.getResources().getString(R.string.needsFillFields), Toast.LENGTH_SHORT).show();
                    } else {
                        newRowView.view.setVisibility(View.INVISIBLE);
                        addButtonNewState = !addButtonNewState;
                        bu_Add.setText(getResources().getString(R.string.newbutton));

                        //добавляем в базу
                        customerProcessor.CreateCustomer(newRowView.GetCustomerModel());

                        Toast.makeText(context, context.getResources().getString(R.string.haveAdded), Toast.LENGTH_SHORT).show();
                        getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    }
                }

                break;
            case R.id.button_delete:
                if (selectedView == null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    customerProcessor.DeleteCustomerById(Integer.parseInt(selectedView.id.getText().toString()));
                    Toast.makeText(context, context.getResources().getString(R.string.haveDeleted), Toast.LENGTH_SHORT).show();
                    getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    selectedView = null;
                }

                break;
            case R.id.button_update:
                if (selectedView==null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    Bundle b= new Bundle();
                    b.putSerializable(CustomerModel.Bundle, selectedView.GetCustomerModel());
                    UpdateCustomerFragment uf = new UpdateCustomerFragment(this);
                    uf.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, uf)
                            .addToBackStack(null).commit();
                }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.d(Prefs.LOG, "on create loader");
        Loader<Cursor> cursor = null;
        switch (i){
            case DATABASE_LOADER:
                cursor = new CustomerProcessor.CursorReader(getActivity());
                break;
        }
        return cursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        customerAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) { }
}
