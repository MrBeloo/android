package com.example.app.Fragments.dialog;

import com.example.app.Models.OrderModel;

/**
 * Created by Beloo on 07.02.14.
 */
public interface IOrderUpdateListener extends IOrderListener {
    @Override
    void ProcessOrder(OrderModel mOrder);
}
