package com.example.app.Fragments.dialog;

import com.example.app.Models.VendorModel;

/**
 * Created by dev2 on 2/7/14.
 */
public interface IUpdateVendorListener {
    public void UpdateClicked(VendorModel mVendor);
}
