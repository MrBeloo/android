package com.example.app.Models;

import java.io.Serializable;

/**
 * Created by dev2 on 04/02/14.
 */
public class ProductModel implements Serializable{
    public Integer id;
    public String productName;
    public Float price;
    public String manufacturer;

    public static String ProductName = "name";
    public static String Price = "price";
    public static String Manufacturer ="manufacturer";
    public static String Id ="_id";
    public static String Bundle="mProduct";
}
