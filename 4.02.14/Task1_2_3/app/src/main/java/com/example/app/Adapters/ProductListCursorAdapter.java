package com.example.app.Adapters;

import android.content.Context;

import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.app.Models.ProductModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.ProductItemVM;


public class ProductListCursorAdapter extends CursorAdapter {
    public ProductListCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        lInflater = (LayoutInflater.from(context));
    }

    LayoutInflater lInflater;

    public int getPosition(int cursorId){
        Log.d(Prefs.LOG, "selId = "+ cursorId);
        for (int i=0; i< getCount(); i++){
            Log.d(Prefs.LOG, String.valueOf(getItemId(i)));
            if (getItemId(i) == cursorId){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Log.d(Prefs.LOG, "before cursor reading");
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        ProductItemVM<TextView> productView;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.product_db_view, parent, false);
            productView = new ProductItemVM<TextView>(convertView);

            convertView.setTag(productView);
        } else {
            productView = (ProductItemVM<TextView>)convertView.getTag();
        }

        Cursor cursor = (Cursor)getItem(position);

        Log.d(Prefs.LOG,"here loads cursor view");

        productView.id.setText(cursor.getString(cursor.getColumnIndex(ProductModel.Id)));
        productView.productName.setText(cursor.getString(cursor.getColumnIndex(ProductModel.ProductName)));
        productView.price.setText(cursor.getString(cursor.getColumnIndex(ProductModel.Price)));
        productView.manufacturer.setText(cursor.getString(cursor.getColumnIndex(ProductModel.Manufacturer)));

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

        Log.d(Prefs.LOG, "new view");

        return lInflater.inflate(R.layout.product_db_view, viewGroup,false);
    }
}
