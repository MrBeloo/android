package com.example.app.Fragments.dialog;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.app.Adapters.CustomerListCursorAdapter;
import com.example.app.Adapters.ProductListCursorAdapter;
import com.example.app.Adapters.VendorListCursorAdapter;
import com.example.app.DbProcessors.CustomerProcessor;
import com.example.app.DbProcessors.ProductProcessor;
import com.example.app.DbProcessors.VendorProcessor;
import com.example.app.Models.OrderModel;
import com.example.app.Prefs;
import com.example.app.R;

/**
 * Created by dev2 on 2/7/14.
 */
public class OrderProcessFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final int PRODUCTS_LOADER = 5;
    private static final int VENDORS_LOADER = 6;
    private static final int CUSTOMERS_LOADER = 7;

    IOrderListener iListener;
    Mode mode;

    Spinner productNameSpinner;
    Spinner vendorNameSpinner;
    Spinner customerNameSpinner;

    ProductListCursorAdapter productsAdapter;
    CustomerListCursorAdapter customerAdapter;
    VendorListCursorAdapter vendorAdapter;

    OrderModel selectedOrder;

    public enum Mode{
        ADD, UPDATE
    }

    public OrderProcessFragment(IOrderListener iListener, Mode mode) {
        this.iListener = iListener;
        this.mode = mode;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.order_edit, container, false);

        productNameSpinner = (Spinner)v.findViewById(R.id.spinner_productName);
        vendorNameSpinner = (Spinner)v.findViewById(R.id.spinner_vendorName);
        customerNameSpinner = (Spinner)v.findViewById(R.id.spinner_customerName);
        Button button = (Button)v.findViewById(R.id.button_update);

        Bundle b = getArguments();
        if (b==null){
            selectedOrder = new OrderModel();
        } else {
            selectedOrder = (OrderModel)b.getSerializable(OrderModel.Bundle);
        }
        productsAdapter =  new ProductListCursorAdapter(inflater.getContext(), null, true);
        customerAdapter = new CustomerListCursorAdapter(inflater.getContext(), null, true);
        vendorAdapter = new VendorListCursorAdapter(inflater.getContext(), null, true);
        customerNameSpinner.setAdapter(customerAdapter);
        productNameSpinner.setAdapter(productsAdapter);
        vendorNameSpinner.setAdapter(vendorAdapter);

        if ( mode == Mode.ADD ){
            button.setText(getResources().getString(R.string.add));
        }

        getLoaderManager().initLoader(PRODUCTS_LOADER, null, this);
        getLoaderManager().initLoader(VENDORS_LOADER, null, this).forceLoad();
        getLoaderManager().initLoader(CUSTOMERS_LOADER, null, this).forceLoad();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View productView = productNameSpinner.getSelectedView();
                TextView productId = (TextView)productView.findViewById(R.id.textView_id);
                View customerView = customerNameSpinner.getSelectedView();
                TextView customerId = (TextView)customerView.findViewById(R.id.textView_id);
                View vendorView = vendorNameSpinner.getSelectedView();
                TextView vendorId = (TextView)vendorView.findViewById(R.id.textView_id);
                selectedOrder.productId = Integer.parseInt(productId.getText().toString());
                selectedOrder.customerId = Integer.parseInt(customerId.getText().toString());
                selectedOrder.vendorId = Integer.parseInt(vendorId.getText().toString());
                iListener.ProcessOrder(selectedOrder);
            }
        });

        return v;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Loader<Cursor> cursor = null;
        switch (i){
            case PRODUCTS_LOADER:
                Log.d(Prefs.LOG, "on products database loader");
                cursor = new ProductProcessor.CursorReader(getActivity());
                break;
            case CUSTOMERS_LOADER:
                Log.d(Prefs.LOG, "on products database loader");
                cursor = new CustomerProcessor.CursorReader(getActivity());
                break;
            case VENDORS_LOADER:
                Log.d(Prefs.LOG, "on products database loader");
                cursor = new VendorProcessor.CursorReader(getActivity());
                break;
        }
        return cursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {

        switch (cursorLoader.getId()){
            case PRODUCTS_LOADER:
                productsAdapter.swapCursor(cursor);
                if (mode == Mode.UPDATE)
                    productNameSpinner.setSelection(productsAdapter.getPosition(selectedOrder.productId));
                break;
            case CUSTOMERS_LOADER:
                customerAdapter.swapCursor(cursor);
                if (mode == Mode.UPDATE)
                    customerNameSpinner.setSelection(customerAdapter.getPosition(selectedOrder.customerId));
                //Log.d(Prefs.LOG, "customerId picked" + customerAdapter.getPosition(selectedOrder.customerId));

                break;
            case VENDORS_LOADER:
                vendorAdapter.swapCursor(cursor);
                if (mode == Mode.UPDATE)
                    vendorNameSpinner.setSelection(vendorAdapter.getPosition(selectedOrder.vendorId));
                //Log.d(Prefs.LOG, "vendorId picked" + vendorAdapter.getPosition(selectedOrder.vendorId));
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}
