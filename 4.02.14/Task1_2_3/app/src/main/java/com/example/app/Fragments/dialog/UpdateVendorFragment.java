package com.example.app.Fragments.dialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.Models.VendorModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.CustomerItemVM;
import com.example.app.ViewModels.VendorItemVM;

public class UpdateVendorFragment extends Fragment {

    IUpdateVendorListener iListener;
    VendorModel mVendor;
    VendorItemVM<EditText> vendorView;

    public UpdateVendorFragment(IUpdateVendorListener iListener) {
        this.iListener = iListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.vendor_update, container, false);

        vendorView = new VendorItemVM<EditText>(v);
        Button bu_update = (Button)v.findViewById(R.id.button_update);

        Bundle b = getArguments();
        mVendor = (VendorModel)b.getSerializable(VendorModel.Bundle);
        vendorView.id.setText(mVendor.id.toString());
        vendorView.name.setText(mVendor.name);

        bu_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iListener.UpdateClicked(vendorView.GetCustomerModel());
            }
        });

        return v;
    }
}
