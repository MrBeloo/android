package com.example.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.app.Models.VendorModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.VendorItemVM;

/**
* Created by dev2 on 2/7/14.
*/
public class VendorListCursorAdapter extends CursorAdapter {
    public VendorListCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        lInflater = (LayoutInflater.from(context));
    }

    LayoutInflater lInflater;

    @Override
    public void bindView(View view, Context context, Cursor cursor) {}

    public int getPosition(int cursorId){
        Log.d(Prefs.LOG, "selId = "+ cursorId);
        for (int i=0; i< getCount(); i++){
            Log.d(Prefs.LOG, String.valueOf(getItemId(i)));
            if (getItemId(i) == cursorId){
                return i;
            }
        }
        return -1;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        VendorItemVM<TextView> vendorView;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.vendor_db_view, parent, false);
            vendorView = new VendorItemVM<TextView>(convertView);

            convertView.setTag(vendorView);
        } else {
            vendorView = (VendorItemVM<TextView>)convertView.getTag();
        }

        Cursor cursor = (Cursor)getItem(position);

        vendorView.id.setText(cursor.getString(cursor.getColumnIndex(VendorModel.Id)));
        vendorView.name.setText(cursor.getString(cursor.getColumnIndex(VendorModel.Name)));

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return lInflater.inflate(R.layout.vendor_db_view, viewGroup,false);
    }
}
