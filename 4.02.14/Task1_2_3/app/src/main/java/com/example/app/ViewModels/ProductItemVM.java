package com.example.app.ViewModels;

import android.view.View;
import android.widget.TextView;

import com.example.app.Models.ProductModel;
import com.example.app.R;

/**
 * Created by Beloo on 09.02.14.
 */

public class ProductItemVM<T extends TextView> {

    public View view;
    public TextView productName;
    public TextView price;
    public TextView manufacturer;
    public TextView id;

    public ProductItemVM(View view){
        this.view = view;
        id = (T) view.findViewById(R.id.textView_id);
        productName = (T) view.findViewById(R.id.textView_productName);
        price = (T) view.findViewById(R.id.textView_price);
        manufacturer = (T)view.findViewById(R.id.textView_manufacturer);
    }

    public boolean IsEmpty(){
        return productName.getText().toString().matches("") ||
                price.getText().toString().matches("") || manufacturer.getText().toString().matches("");
    }

    public ProductModel GetProductModel(){
        ProductModel mProduct = new ProductModel();
        mProduct.id = Integer.parseInt(id.getText().toString());
        mProduct.productName = productName.getText().toString();
        mProduct.price = Float.parseFloat(price.getText().toString());
        mProduct.manufacturer = manufacturer.getText().toString();
        return mProduct;
    }

    public void FromProductModel(ProductModel mProduct){

    }
}