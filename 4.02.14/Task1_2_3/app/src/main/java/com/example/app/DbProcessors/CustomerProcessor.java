package com.example.app.DbProcessors;


import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.example.app.Models.CustomerModel;
import com.example.app.Models.DbModel;
import com.example.app.Prefs;

/**
 * Created by dev2 on 04/02/14.
 */
public
class CustomerProcessor {

    private static volatile CustomerProcessor instance;

    public static CustomerProcessor getInstance() {
        if (instance == null) {
            synchronized (CustomerProcessor.class) {
                if (instance == null) {
                    instance = new CustomerProcessor();
                }
            }
        }
        return instance;
    }

    private CustomerProcessor(){}

    private class AsyncTaskDelete extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(Integer... value) {
            int delCount = DatabaseHelper.getInstance().getWritableDatabase()
                    .delete(DbModel.Customers, CustomerModel.Id + " = " + value[0], null);
            return delCount;
        }
    }

    public void DeleteCustomerById(int id){
        new AsyncTaskDelete().execute(id);
    }

    private class AsyncTaskUpdate extends AsyncTask<CustomerModel, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(CustomerModel... value) {
            CustomerModel mCustomer = value[0];
            int updCount=0;
            if (mCustomer.id > 0){
                updCount = DatabaseHelper.getInstance().getWritableDatabase()
                        .update(DbModel.Customers, DatabaseHelper.CustomerToCv(mCustomer), CustomerModel.Id + " = ?",
                        new String[] { mCustomer.id.toString() });
                Log.d(Prefs.LOG, "updated rows count = " + updCount);
            }
            return updCount;
        }
    }

    public void UpdateCustomerById (CustomerModel mCustomer){
        new AsyncTaskUpdate().execute(mCustomer);
    }



    private class AsyncTaskCreate extends AsyncTask<CustomerModel, Void, Void> {

        @Override
        protected synchronized Void doInBackground(CustomerModel... value) {
            DatabaseHelper.getInstance().getWritableDatabase()
                    .insert(DbModel.Customers, null, DatabaseHelper.CustomerToCv(value[0]));
            return null;
        }
    }

    public void CreateCustomer (CustomerModel mCustomer){
        new AsyncTaskCreate().execute(mCustomer);
    }

    public static class CursorReader extends CursorLoader {

        public CursorReader(Context context) {
            super(context);
        }

        @Override
        public synchronized Cursor loadInBackground() {
            return DatabaseHelper.getInstance().getReadableDatabase()
                    .query(DbModel.Customers, null, null, null, null, null, null);
        }
    }

    public synchronized long GetLastId(){
        long lastId=0;
        Cursor c = DatabaseHelper.getInstance().getWritableDatabase()
                .rawQuery("SELECT MAX("+ CustomerModel.Id+") FROM " + DbModel.Customers, null);
        if (c != null && c.moveToFirst())
        {
            lastId = c.getLong(0);
        }
        return lastId;
    }
}
