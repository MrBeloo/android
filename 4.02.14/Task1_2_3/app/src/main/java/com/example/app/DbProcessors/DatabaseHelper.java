package com.example.app.DbProcessors;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.app.MainActivity;
import com.example.app.Models.CustomerModel;
import com.example.app.Models.DbModel;
import com.example.app.Models.OrderModel;
import com.example.app.Models.ProductModel;
import com.example.app.Models.VendorModel;

import java.nio.channels.NotYetConnectedException;

/**
* Created by dev2 on 2/5/14.
*/
public class DatabaseHelper extends SQLiteOpenHelper {

    public static Context context;

    private static DatabaseHelper instance;

    public static synchronized DatabaseHelper getInstance() {
        if (context == null){
            throw new NotYetConnectedException();
        }
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    private DatabaseHelper(Context context) {
        super(context, DbModel.DB_name, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(MainActivity.LOG, "--- onCreate database ---");
        sqLiteDatabase.execSQL("create table products ("
                + "_id integer primary key autoincrement,"
                + "name text,"
                + "price FLOAT(6,2),"
                + "manufacturer text,"
                + "stock int"+ ");");
        sqLiteDatabase.execSQL("create table vendors ("
                + "_id integer primary key autoincrement,"
                + "name text"  + ");");
        sqLiteDatabase.execSQL("create table customers ("
                + "_id integer primary key autoincrement,"
                + "name text,"
                + "phone text,"
                + "address text"+ ");");
        sqLiteDatabase.execSQL("create table orders ("
                + "_id integer primary key autoincrement,"
                + "productId int,"
                + "vendorId int,"
                + "customerId int,"
                + "FOREIGN KEY(productId) REFERENCES products(_id) ON DELETE CASCADE,"
                + "FOREIGN KEY(vendorId) REFERENCES vendors(_id) ON DELETE CASCADE,"
                + "FOREIGN KEY(customerId) REFERENCES customers(_id) ON DELETE CASCADE"+ ");");

        InitDb(sqLiteDatabase);
    }

    public static ContentValues CustomerToCv(CustomerModel mCustomer){
        ContentValues cv;
        cv = new ContentValues();
        cv.put(CustomerModel.Name, mCustomer.name);
        cv.put(CustomerModel.Address, mCustomer.address);
        cv.put(CustomerModel.Phone, mCustomer.phone);
        return cv;
    }

    public static ContentValues ProductToCv(ProductModel mProduct){
        ContentValues cv;
        cv = new ContentValues();
        cv.put(ProductModel.ProductName,mProduct.productName);
        cv.put(ProductModel.Manufacturer, mProduct.manufacturer);
        cv.put(ProductModel.Price, mProduct.price);
        return cv;
    }

    public static ContentValues VendorToCv (VendorModel mVendor){
        ContentValues cv;
        cv = new ContentValues();
        cv.put(VendorModel.Name, mVendor.name);
        return cv;
    }

    public static ContentValues OrderToCv (OrderModel mOrder){
        ContentValues cv;
        cv = new ContentValues();
        cv.put(OrderModel.VendorId, mOrder.vendorId);
        cv.put(OrderModel.ProductId, mOrder.productId);
        cv.put(OrderModel.CustomerId, mOrder.customerId);
        return cv;
    }

    private void InitDb(SQLiteDatabase db){

        //products init
        ContentValues cv;
        cv = new ContentValues();
        cv.put("name","cake");
        cv.put("price",5.5);
        cv.put("manufacturer","grandma bakery");
        cv.put("stock", 6155);
        db.insert("products",null,cv);

        cv = new ContentValues();
        cv.put("name","pie");
        cv.put("price",75);
        cv.put("manufacturer","grandma bakery");
        cv.put("stock", 379);
        db.insert("products",null,cv);

        cv = new ContentValues();
        cv.put("name","ApplePie");
        cv.put("price",555);
        cv.put("manufacturer","grandma bakery");
        cv.put("stock", 1);
        db.insert("products",null,cv);

        cv = new ContentValues();
        cv.put("name","cake");
        cv.put("price",10.5);
        cv.put("manufacturer","HotCakes");
        cv.put("stock", 4125);
        db.insert("products",null,cv);

        cv = new ContentValues();
        cv.put("name","pie");
        cv.put("price",70);
        cv.put("manufacturer","HotCakes");
        cv.put("stock", 301);
        db.insert("products",null,cv);

        cv = new ContentValues();
        cv.put("name","ApplePie");
        cv.put("price",200);
        cv.put("manufacturer","HotCakes");
        cv.put("stock", 77);
        db.insert("products",null,cv);

        //vendors init
        cv = new ContentValues();
        cv.put("name","Mike");
        db.insert("vendors",null,cv);

        cv = new ContentValues();
        cv.put("name","John");
        db.insert("vendors",null,cv);

        //customers init
        cv = new ContentValues();
        cv.put("name","Katya");
        cv.put("phone","000-000-000");
        cv.put("address","Somewhere, 37");
        db.insert("customers", null, cv);

        cv = new ContentValues();
        cv.put("name","Nick");
        cv.put("phone","000-000-000");
        cv.put("address","Somewhere, 36");
        db.insert("customers",null,cv);

        cv = new ContentValues();
        cv.put("name","Clod");
        cv.put("phone","000-000-000");
        cv.put("address","Somewhere, 35");
        db.insert("customers",null,cv);

        // orders init
        cv = new ContentValues();
        cv.put("productId",1);
        cv.put("vendorId",1);
        cv.put("customerId",1);
        db.insert("orders",null,cv);

        cv = new ContentValues();
        cv.put("productId",3);
        cv.put("vendorId",1);
        cv.put("customerId",2);
        db.insert("orders",null,cv);

        cv = new ContentValues();
        cv.put("productId",6);
        cv.put("vendorId",2);
        cv.put("customerId",3);
        db.insert("orders",null,cv);

        cv = new ContentValues();
        cv.put("productId",2);
        cv.put("vendorId",2);
        cv.put("customerId",1);
        db.insert("orders",null,cv);

        cv = new ContentValues();
        cv.put("productId",3);
        cv.put("vendorId",1);
        cv.put("customerId",2);
        db.insert("orders",null,cv);

        cv = new ContentValues();
        cv.put("productId",4);
        cv.put("vendorId",2);
        cv.put("customerId",3);
        db.insert("orders",null,cv);

    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }
}
