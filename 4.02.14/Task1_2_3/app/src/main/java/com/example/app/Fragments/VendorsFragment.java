package com.example.app.Fragments;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.Adapters.VendorListCursorAdapter;
import com.example.app.DbProcessors.VendorProcessor;
import com.example.app.Fragments.dialog.IUpdateVendorListener;
import com.example.app.Fragments.dialog.UpdateVendorFragment;
import com.example.app.Models.VendorModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.VendorItemVM;

/**
 * Created by dev2 on 04/02/14.
 */
public class VendorsFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>, IUpdateVendorListener {

    private static final int DATABASE_LOADER = 3;

    private VendorListCursorAdapter vendorAdapter;
    private Context context;
    private Button bu_Update;
    private Button bu_Add;
    private ListView customersListView;
    private LayoutInflater inflater;

    private Boolean addButtonNewState;
    private VendorItemVM<EditText> newRowView;
    private VendorItemVM<TextView> selectedView;

    private VendorProcessor vendorProcessor;

    @Override
    public void UpdateClicked(VendorModel mCustomer) {
        Log.d(Prefs.LOG,"before database update");
        vendorProcessor.UpdateVendorById(mCustomer);
        getFragmentManager().popBackStack();
        Toast.makeText(context, getResources().getString(R.string.haveUpdated), Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        addButtonNewState = true;
        this.inflater = inflater;
        context = getActivity();
        View view = inflater.inflate(R.layout.database_fragment, null);
        vendorProcessor = VendorProcessor.getInstance();

        Log.d(Prefs.LOG,"customer fragment view");

        bu_Add = (Button)view.findViewById(R.id.button_add);
        Button bu_Delete = (Button)view.findViewById(R.id.button_delete);
        bu_Update = (Button)view.findViewById(R.id.button_update);
        customersListView = (ListView)view.findViewById(R.id.listView_elements);
        VendorItemVM<TextView> header = new VendorItemVM(inflater.inflate(R.layout.vendor_db_view, container, false));
        header.id.setText(VendorModel.Id);
        header.name.setText(VendorModel.Name);

        getFragmentManager().beginTransaction().add(R.id.description_layout, new DescriptionsFragment(header.view)).commit();

        Log.d(Prefs.LOG, "before create cursor");

        vendorAdapter = new VendorListCursorAdapter(context, null, true);
        newRowView = new VendorItemVM(inflater.inflate(R.layout.vendor_db_edit, null, false));
        newRowView.view.setVisibility(View.INVISIBLE);
        customersListView.addFooterView(newRowView.view);
        customersListView.setAdapter(vendorAdapter);

        customersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(Prefs.LOG, "clicked");
                selectedView = new VendorItemVM<TextView>(view);
                view.setSelected(true);
            }
        });
        bu_Add.setOnClickListener(this);
        bu_Delete.setOnClickListener(this);
        bu_Update.setOnClickListener(this);

        getLoaderManager().initLoader(DATABASE_LOADER, null, this).forceLoad();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_add:
                if (addButtonNewState){
                    addButtonNewState = !addButtonNewState;
                    bu_Add.setText(getResources().getString(R.string.add));
                    newRowView.view.setVisibility(View.VISIBLE);
                    newRowView.id.setText(String.valueOf(vendorProcessor.GetLastId() + 1));
                    customersListView.setSelection(vendorAdapter.getCount() - 1);
                } else {
                    if (newRowView.IsEmpty()){
                        Toast.makeText(context, context.getResources().getString(R.string.needsFillFields), Toast.LENGTH_SHORT).show();
                    } else {
                        addButtonNewState = !addButtonNewState;
                        newRowView.view.setVisibility(View.INVISIBLE);
                        bu_Add.setText(getResources().getString(R.string.newbutton));

                        //добавляем в базу
                        vendorProcessor.CreateVendor(newRowView.GetCustomerModel());

                        Toast.makeText(context, context.getResources().getString(R.string.haveAdded), Toast.LENGTH_SHORT).show();
                        getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    }
                }

                break;
            case R.id.button_delete:
                if (selectedView == null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    vendorProcessor.DeleteVendorById(Integer.parseInt(selectedView.id.getText().toString()));
                    Toast.makeText(context, context.getResources().getString(R.string.haveDeleted), Toast.LENGTH_SHORT).show();
                    getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    selectedView = null;
                }

                break;
            case R.id.button_update:
                if (selectedView==null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    Bundle b= new Bundle();
                    b.putSerializable(VendorModel.Bundle, selectedView.GetCustomerModel());
                    UpdateVendorFragment uf = new UpdateVendorFragment(this);
                    uf.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, uf)
                            .addToBackStack(null).commit();
                }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Log.d(Prefs.LOG, "on create loader");
        Loader<Cursor> cursor = null;
        switch (i){
            case DATABASE_LOADER:
                cursor = new VendorProcessor.CursorReader(getActivity());
                break;
        }
        return cursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        vendorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) { }
}
