package com.example.app.Fragments.dialog;

import com.example.app.Models.CustomerModel;

/**
 * Created by dev2 on 2/7/14.
 */
public interface IUpdateCustomerListener {
    public void UpdateClicked(CustomerModel mCustomer);
}
