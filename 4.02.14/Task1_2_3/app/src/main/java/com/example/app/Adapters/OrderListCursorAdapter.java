package com.example.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.app.Models.OrderModel;
import com.example.app.Models.VendorModel;
import com.example.app.R;
import com.example.app.ViewModels.OrderItemViewModel;

/**
 * Created by dev2 on 2/7/14.
 */
public class OrderListCursorAdapter extends CursorAdapter {
    public OrderListCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        lInflater = (LayoutInflater.from(context));
    }

    LayoutInflater lInflater;

    @Override
    public void bindView(View view, Context context, Cursor cursor) {}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderItemViewModel viewHolder;

        if (convertView == null){
            convertView = lInflater.inflate(R.layout.order_db_view, parent, false);
            viewHolder = new OrderItemViewModel(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (OrderItemViewModel)convertView.getTag();
        }

        Cursor cursor = (Cursor)getItem(position);

        viewHolder.id.setText(cursor.getString(cursor.getColumnIndex(OrderModel.Id)));
        viewHolder.productName.setText(cursor.getString(cursor.getColumnIndex("prName")));
        viewHolder.vendorName.setText(cursor.getString(cursor.getColumnIndex("veName")));
        viewHolder.customerName.setText(cursor.getString(cursor.getColumnIndex("cuName")));

        return convertView;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return lInflater.inflate(R.layout.order_db_view, viewGroup,false);
    }
}
