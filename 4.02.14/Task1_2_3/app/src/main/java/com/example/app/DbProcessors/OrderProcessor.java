package com.example.app.DbProcessors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.example.app.Models.CustomerModel;
import com.example.app.Models.DbModel;
import com.example.app.Models.OrderModel;
import com.example.app.Models.ProductModel;
import com.example.app.Models.ReadQueryModel;
import com.example.app.Models.VendorModel;
import com.example.app.Prefs;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by dev2 on 04/02/14.
 */
public class OrderProcessor {

    private static String TABLE_NAME = DbModel.Orders;

    private static volatile OrderProcessor instance;

    public static OrderProcessor getInstance() {
        if (instance == null) {
            synchronized (OrderProcessor.class) {
                if (instance == null) {
                    instance =  new OrderProcessor();
                }
            }
        }
        return instance;
    }

    private OrderProcessor(){}

    private class AsyncTaskDelete extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(Integer... value) {
            int delCount = DatabaseHelper.getInstance().getWritableDatabase()
                    .delete(TABLE_NAME, OrderModel.Id + " = " + value[0], null);
            return delCount;
        }
    }

    public void DeleteOrderById(int id){
        new AsyncTaskDelete().execute(id);
    }

    private class AsyncTaskUpdate extends AsyncTask<OrderModel, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(OrderModel... value) {
            OrderModel mOrder = value[0];
            int updCount=0;
            if (mOrder.id > 0){
                updCount = DatabaseHelper.getInstance().getWritableDatabase()
                        .update(TABLE_NAME, DatabaseHelper.OrderToCv(mOrder), OrderModel.Id + " = ?",
                        new String[] { mOrder.id.toString() });
                Log.d(Prefs.LOG, "updated rows count = " + updCount);
            }
            return updCount;
        }
    }

    public void UpdateOrderById (OrderModel mOrder){
        new AsyncTaskUpdate().execute(mOrder);
    }

    private class AsyncTaskCreate extends AsyncTask<OrderModel, Void, Void> {

        @Override
        protected synchronized Void doInBackground(OrderModel ... value) {
            DatabaseHelper.getInstance().getWritableDatabase()
                    .insert(TABLE_NAME, null, DatabaseHelper.OrderToCv(value[0]));
            return null;
        }
    }

    public void CreateOrder (OrderModel mOrder){
        new AsyncTaskCreate().execute(mOrder);
    }

    public static class CursorReader extends CursorLoader {

        ReadQueryModel mQuery;

        public CursorReader(Context context) {
            super(context);
        }

        @Override
        public synchronized Cursor loadInBackground() {
            String sqlQuery = "SELECT ORD._id, PR._id as productId, VE._id as vendorId, CU._id as customerId, " +
                        "PR.name as prName, VE.name as veName, CU.name as cuName "
                    + "FROM orders as ORD "
                    + "INNER JOIN products as PR ON ORD.productId=PR._id "
                    + "INNER JOIN vendors as VE ON ORD.vendorId=VE._id "
                    + "INNER JOIN customers as CU ON ORD.customerId=CU._id";
            return DatabaseHelper.getInstance().getReadableDatabase()
                    .rawQuery(sqlQuery, null);
        }
    }

    public synchronized long GetLastId(){
        long lastId=0;
        Cursor c = DatabaseHelper.getInstance().getWritableDatabase()
                .rawQuery("SELECT MAX("+ OrderModel.Id+") FROM " + TABLE_NAME, null);
        if (c != null && c.moveToFirst())
        {
            lastId = c.getLong(0);
        }
        return lastId;
    }
}
