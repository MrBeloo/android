package com.example.app.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app.Adapters.OrderListCursorAdapter;
import com.example.app.DbProcessors.OrderProcessor;
import com.example.app.Fragments.dialog.OrderProcessFragment;
import com.example.app.Fragments.dialog.IAddOrderListener;
import com.example.app.Fragments.dialog.IOrderUpdateListener;
import com.example.app.Models.OrderModel;
import com.example.app.Prefs;
import com.example.app.R;
import com.example.app.ViewModels.OrderItemViewModel;

/**
 * Created by dev2 on 04/02/14.
 */
public class OrdersFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>{

    private static final int DATABASE_LOADER = 3;

    private OrderListCursorAdapter orderAdapter;
    private Context context;
    private Button bu_Update;
    private Button bu_Add;
    private ListView customersListView;
    private LayoutInflater inflater;

    private OrderProcessor orderProcessor;
    private OrderModel selected;

    private class OrderAddController implements IAddOrderListener{
        @Override
        public void ProcessOrder(OrderModel mOrder) {
            orderProcessor.CreateOrder(mOrder);
            getFragmentManager().popBackStack();
            Toast.makeText(context, context.getResources().getString(R.string.haveAdded), Toast.LENGTH_SHORT).show();
        }
    }

    private class OrderUpdateController implements IOrderUpdateListener{
        @Override
        public void ProcessOrder(OrderModel mOrder) {
            orderProcessor.UpdateOrderById(mOrder);
            getFragmentManager().popBackStack();
            Toast.makeText(context, getResources().getString(R.string.haveUpdated), Toast.LENGTH_SHORT).show();
        }
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.inflater = inflater;
        context = getActivity();
        View view = inflater.inflate(R.layout.database_fragment, null);
        orderProcessor = OrderProcessor.getInstance();

        Log.d(Prefs.LOG,"customer fragment view");

        bu_Add = (Button)view.findViewById(R.id.button_add);
        Button bu_Delete = (Button)view.findViewById(R.id.button_delete);
        bu_Update = (Button)view.findViewById(R.id.button_update);
        customersListView = (ListView)view.findViewById(R.id.listView_elements);
        OrderItemViewModel header = new OrderItemViewModel(inflater.inflate(R.layout.order_db_view, container, false));
        header.id.setText(OrderModel.Id);
        header.productName.setText("Product Name");
        header.customerName.setText("Customer Name");
        header.vendorName.setText("Vendor Name");
        bu_Add.setText(getResources().getString(R.string.add));

        getFragmentManager().beginTransaction().add(R.id.description_layout, new DescriptionsFragment(header.view)).commit();

        Log.d(Prefs.LOG, "before create cursor");

        orderAdapter = new OrderListCursorAdapter(context, null, true);
        customersListView.setAdapter(orderAdapter);

        customersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Cursor c =  orderAdapter.getCursor();
                selected = new OrderModel();
                selected.id = Integer.parseInt(((TextView)view.findViewById(R.id.textView_id)).getText().toString());
                selected.productId = Integer.parseInt(c.getString(c.getColumnIndex(OrderModel.ProductId)));
                selected.vendorId = Integer.parseInt(c.getString(c.getColumnIndex(OrderModel.VendorId)));
                selected.customerId = Integer.parseInt(c.getString(c.getColumnIndex(OrderModel.CustomerId)));
                view.setSelected(true);
            }
        });
        bu_Add.setOnClickListener(this);
        bu_Delete.setOnClickListener(this);
        bu_Update.setOnClickListener(this);

        getLoaderManager().initLoader(DATABASE_LOADER, null, this).forceLoad();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_add:
                Log.d(Prefs.LOG,"button add clicked");
                getFragmentManager().beginTransaction()
                        .replace(R.id.container, new OrderProcessFragment(new OrderAddController(), OrderProcessFragment.Mode.ADD))
                        .addToBackStack(null).commit();

                break;
            case R.id.button_delete:
                if (selected == null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    orderProcessor.DeleteOrderById(selected.id);
                    Toast.makeText(context, context.getResources().getString(R.string.haveDeleted), Toast.LENGTH_SHORT).show();
                    getLoaderManager().restartLoader(DATABASE_LOADER, null, this);
                    selected = null;
                }

                break;
            case R.id.button_update:
                if (selected==null){
                    Toast.makeText(context, context.getResources().getString(R.string.needsPickaRow), Toast.LENGTH_SHORT).show();
                } else {
                    Bundle b = new Bundle();
                    b.putSerializable(OrderModel.Bundle, selected);
                    OrderProcessFragment uf = new OrderProcessFragment(new OrderUpdateController(), OrderProcessFragment.Mode.UPDATE);
                    uf.setArguments(b);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.container, uf)
                            .addToBackStack(null).commit();
                }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        Loader<Cursor> cursor = null;
        switch (i){
            case DATABASE_LOADER:
                Log.d(Prefs.LOG, "on orders database loader");
                cursor = new OrderProcessor.CursorReader(getActivity());
                break;
        }
        return cursor;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        orderAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) { }
}
