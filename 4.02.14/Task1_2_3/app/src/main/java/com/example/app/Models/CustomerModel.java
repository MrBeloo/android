package com.example.app.Models;

import java.io.Serializable;

/**
 * Created by dev2 on 04/02/14.
 */
public class CustomerModel implements Serializable{
    public Integer id;
    public String name;
    public String phone;
    public String address;

    public static String Id = "_id";
    public static String Name = "name";
    public static String Phone = "phone";
    public static String Address = "address";
    public static String Bundle = "mCustomer";
}
