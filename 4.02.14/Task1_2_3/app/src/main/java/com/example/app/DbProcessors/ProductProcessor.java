package com.example.app.DbProcessors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.example.app.Models.DbModel;
import com.example.app.Models.ProductModel;
import com.example.app.Models.ReadQueryModel;
import com.example.app.Prefs;

/**
 * Created by dev2 on 04/02/14.
 */
public class ProductProcessor {

    private static volatile ProductProcessor instance;

    public static ProductProcessor getInstance() {
        if (instance == null) {
            synchronized (ProductProcessor.class) {
                if (instance == null) {
                    instance = new ProductProcessor();
                }
            }
        }
        return instance;
    }

    private ProductProcessor(){}

    private class AsyncTaskDelete extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(Integer... value) {
            int delCount = DatabaseHelper.getInstance().getWritableDatabase()
                    .delete(DbModel.Products, ProductModel.Id + " = " + value[0], null);
            return delCount;
        }
    }

    //product process

    public void DeleteProductById(int id){
        new AsyncTaskDelete().execute(id);
    }

    private class AsyncTaskUpdate extends AsyncTask<ProductModel, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(ProductModel... value) {
            ProductModel mProduct = value[0];
            int updCount=0;

            updCount = DatabaseHelper.getInstance().getWritableDatabase()
                    .update(DbModel.Products, DatabaseHelper.ProductToCv(mProduct), ProductModel.Id + " = ?",
                    new String[] { mProduct.id.toString() });
            Log.d(Prefs.LOG, "updated rows count = " + updCount);

            return updCount;
        }
    }

    public void UpdateProductById (ProductModel mProduct){
        new AsyncTaskUpdate().execute(mProduct);
    }

    private class AsyncTaskCreate extends AsyncTask<ProductModel, Void, Void> {

        @Override
        protected synchronized Void doInBackground(ProductModel... value) {
            DatabaseHelper.getInstance().getWritableDatabase()
                    .insert(DbModel.Products, null, DatabaseHelper.ProductToCv(value[0]));
            return null;
        }
    }

    public void CreateProduct (ProductModel mProduct){
        new AsyncTaskCreate().execute(mProduct);
    }

    public static class CursorReader extends CursorLoader {

        public CursorReader(Context context) {
            super(context);
        }

        @Override
        public synchronized Cursor loadInBackground() {
            Log.d(Prefs.LOG, "cursor product loader");

            return DatabaseHelper.getInstance().getReadableDatabase()
                    .query(DbModel.Products, null, null, null, null, null, null);
        }
    }

    public synchronized long GetLastId(){
        long lastId=0;
        Cursor c = DatabaseHelper.getInstance().getWritableDatabase()
                .rawQuery("SELECT MAX("+ProductModel.Id+") FROM " + DbModel.Products, null);
        if (c != null && c.moveToFirst())
        {
            lastId = c.getLong(0);
        }
        return lastId;
    }
}
