package com.example.app.DbProcessors;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.example.app.Models.CustomerModel;
import com.example.app.Models.DbModel;
import com.example.app.Models.ProductModel;
import com.example.app.Models.ReadQueryModel;
import com.example.app.Models.VendorModel;
import com.example.app.Prefs;

import java.nio.channels.NotYetConnectedException;

/**
 * Created by dev2 on 04/02/14.
 */
public class VendorProcessor {

    private static String TABLE_NAME = DbModel.Vendors;

    private static volatile VendorProcessor instance;

    public static VendorProcessor getInstance() {
        if (instance == null) {
            synchronized (VendorProcessor.class) {
                if (instance == null) {
                    instance =  new VendorProcessor();
                }
            }
        }
        return instance;
    }

    private VendorProcessor(){}

    private class AsyncTaskDelete extends AsyncTask<Integer, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(Integer... value) {
            int delCount = DatabaseHelper.getInstance().getWritableDatabase()
                    .delete(TABLE_NAME, VendorModel.Id + " = " + value[0], null);
            return delCount;
        }
    }

    public void DeleteVendorById(int id){
        new AsyncTaskDelete().execute(id);
    }

    private class AsyncTaskUpdate extends AsyncTask<VendorModel, Void, Integer> {
        @Override
        protected synchronized Integer doInBackground(VendorModel... value) {
            VendorModel mVendor = value[0];
            int updCount=0;
            if (mVendor.id > 0){
                updCount = DatabaseHelper.getInstance().getWritableDatabase()
                        .update(TABLE_NAME, DatabaseHelper.VendorToCv(mVendor), VendorModel.Id + " = ?",
                        new String[]{mVendor.id.toString()});
                Log.d(Prefs.LOG, "updated rows count = " + updCount);
            }
            return updCount;
        }
    }

    public void UpdateVendorById (VendorModel mVendor){
        new AsyncTaskUpdate().execute(mVendor);
    }

    private class AsyncTaskCreate extends AsyncTask<VendorModel, Void, Void> {

        @Override
        protected synchronized Void doInBackground(VendorModel... value) {
            DatabaseHelper.getInstance().getWritableDatabase()
                    .insert(TABLE_NAME, null, DatabaseHelper.VendorToCv(value[0]));
            return null;
        }
    }

    public void CreateVendor (VendorModel mVendor){
        new AsyncTaskCreate().execute(mVendor);
    }

    public static class CursorReader extends CursorLoader {

        public CursorReader(Context context) {
            super(context);
        }

        @Override
        public synchronized Cursor loadInBackground() {
            return DatabaseHelper.getInstance().getReadableDatabase()
                    .query(TABLE_NAME, null, null, null, null, null, null);
        }
    }

    public synchronized long GetLastId(){
        long lastId=0;
        Cursor c = DatabaseHelper.getInstance().getWritableDatabase()
                .rawQuery("SELECT MAX("+ VendorModel.Id+") FROM " + TABLE_NAME, null);
        if (c != null && c.moveToFirst())
        {
            lastId = c.getLong(0);
        }
        return lastId;
    }
}
