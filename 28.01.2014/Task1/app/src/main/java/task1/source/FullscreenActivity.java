package task1.source;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class FullscreenActivity extends Activity {

    private EditText outText;
    private int REQ_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        outText = (EditText) findViewById(R.id.text_outer);
        String gettedText = getIntent().getStringExtra("get_text");
        outText.setText(gettedText);

    }

    public void onClick(View view){
        Intent intent = new Intent(FullscreenActivity.this, SecondActivity.class);
        startActivityForResult(intent,REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String getted = data.getStringExtra("get_text");
        outText.setText(getted);
    }
}

