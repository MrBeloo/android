package task1.source;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;

public class SecondActivity extends Activity {

    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        text = (EditText) findViewById(R.id.text_input);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    public void onClickSubmitButton(View view) {
        switch (view.getId()) {
            case R.id.submit_button:
                Intent intent = new Intent(this, FullscreenActivity.class);
                intent.putExtra("get_text", text.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }

}
