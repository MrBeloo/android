package com.example.app.models;

import android.database.Cursor;

import com.parse.ParseObject;

import java.io.Serializable;

/**
 * Created by dev2 on 2/10/14.
 */
public class PhotoModel implements Serializable{
    public Integer id;
    public String photoName;
    public double longitude;
    public double latitude;
    public String photoPath;
    public Boolean isDownloaded;
    public String parsePath;

    public static String Id = "_id";
    public static String ParsePath = "parsePath";
    public static String PhotoPath = "photoPath";
    public static String IsDownloaded = "isDownloaded";
    public static String Longitude ="longitude";
    public static String Latitude ="latitude";
    public static String Bundle = "mPhoto";
    public static String PhotoName = "photoName";


    public PhotoModel() {
        this.isDownloaded = false;
        id = 0;
    }

    public PhotoModel (Cursor cursor){
        this();
        if (cursor!= null){
            int i;
            i = cursor.getColumnIndex(PhotoModel.PhotoPath);
            if (i > 0){
                this.photoPath = cursor.getString(i);
            }
            i = cursor.getColumnIndex(PhotoModel.PhotoName);
            if (i > 0){
                this.photoName = cursor.getString(i);
            }
            i = cursor.getColumnIndex(PhotoModel.Longitude);
            if (i > 0){
                this.longitude = cursor.getDouble(i);
            }
            i = cursor.getColumnIndex(PhotoModel.Latitude);
            if (i > 0){
                this.latitude = cursor.getDouble(i);
            }
            i = cursor.getColumnIndex(PhotoModel.IsDownloaded);
            if (i > 0){
                this.isDownloaded = cursor.getInt(i) == 1;
            }
            i = cursor.getColumnIndex(PhotoModel.ParsePath);
            if (i > 0){
                this.parsePath = cursor.getString(i);
            }
            id = cursor.getInt(cursor.getColumnIndex(PhotoModel.Id));
        }
    }

    @Override
    public String toString() {
        return id + " " + photoName + " " + photoPath+ " " + longitude + " " + latitude + " " + isDownloaded;
    }


}
