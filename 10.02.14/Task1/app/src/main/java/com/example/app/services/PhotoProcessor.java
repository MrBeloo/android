package com.example.app.services;

import android.content.Context;

import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.content.CursorLoader;

import com.example.app.models.DbModel;
import com.example.app.models.PhotoModel;
import com.example.app.services.interfaces.IPhotoProcessorListener;

/**
 * Created by dev2 on 2/10/14.
 */
public class PhotoProcessor {
    private static final String TABLE_NAME = DbModel.Photos;
    private static final PhotoProcessor INSTANCE = new PhotoProcessor();

    public static PhotoProcessor getInstance(){
        return INSTANCE;
    }

    private PhotoProcessor(){}

    private class AsyncTaskCreate extends AsyncTask<PhotoModel, Void, Void> {

        IPhotoProcessorListener iListener;

        private AsyncTaskCreate(IPhotoProcessorListener iListener) {
            this.iListener = iListener;
        }

        @Override
        protected synchronized Void doInBackground(PhotoModel... value) {
            DatabaseHelper.getInstance().getWritableDatabase()
                    .insert(TABLE_NAME, null, DatabaseHelper.PhotoToCV(value[0]));
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (iListener !=null)
                iListener.photoStored();
        }
    }

    public void CreatePhoto (PhotoModel mPhoto, IPhotoProcessorListener iListener){
        new AsyncTaskCreate(iListener).execute(mPhoto);
    }

    public static class CursorReader extends CursorLoader {

        public CursorReader(Context context) {
            super(context);
        }

        @Override
        public synchronized Cursor loadInBackground() {
            return DatabaseHelper.getInstance().getReadableDatabase()
                    .query(TABLE_NAME, null, null, null, null, null, null);
        }
    }

    public static class PhotoReaderByPath extends CursorLoader {

        String filePath;

        public PhotoReaderByPath(Context context, String filePath) {
            super(context);
            this.filePath = filePath;
        }

        @Override
        public synchronized Cursor loadInBackground() {

            String [] selectionArgs = new String[]{filePath};
            String sqlQuery = "SELECT *"
                    + " FROM "+DbModel.Photos
                    + " WHERE "+PhotoModel.PhotoPath+" = ?";

            return DatabaseHelper.getInstance().getReadableDatabase()
                    .rawQuery(sqlQuery, selectionArgs);
        }
    }

    private class AsyncTaskUpdate extends AsyncTask<PhotoModel, Void, Void> {
        @Override
        protected synchronized Void doInBackground(PhotoModel... value) {
            PhotoModel mPhoto = value[0];

            DatabaseHelper.getInstance().getWritableDatabase()
                    .update(TABLE_NAME, DatabaseHelper.PhotoToCV(mPhoto), mPhoto.Id + " = ?",
                            new String[] { mPhoto.id.toString() });
            return null;
        }
    }

    public void updatePhotoById(PhotoModel mPhoto){
        new AsyncTaskUpdate().execute(mPhoto);
    }

}
