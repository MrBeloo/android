package com.example.app.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.app.adapters.PhotoCursorAdapter;
import com.example.app.models.PhotoModel;
import com.example.app.helpers.Prefs;
import com.example.app.R;
import com.example.app.services.PhotoProcessor;

/**
 * Created by dev2 on 2/11/14.
 */
public class PhotoGalleryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static int PHOTOS_LOADER = 1;
    PhotoCursorAdapter photoAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.gallery_layout, container, false);
        GridView photosView = (GridView)view.findViewById(R.id.gridView);

        photoAdapter = new PhotoCursorAdapter(getActivity(), null, false);
        photosView.setAdapter(photoAdapter);

        getLoaderManager().initLoader(PHOTOS_LOADER, null, this).forceLoad();

        photosView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(Prefs.LOG, "listener");
                PhotoPreviewFragment photoFragment = new PhotoPreviewFragment();
                PhotoModel mPhoto = new PhotoModel((Cursor)photoAdapter.getItem(i));
                Bundle b = new Bundle();
                b.putSerializable(PhotoModel.Bundle,mPhoto);
                photoFragment.setArguments(b);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, photoFragment)
                        .addToBackStack(null).commit();
            }
        });

        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new PhotoProcessor.CursorReader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.d(Prefs.LOG, "loading finished");
        photoAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}
