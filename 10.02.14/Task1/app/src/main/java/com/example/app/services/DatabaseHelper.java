package com.example.app.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.app.R;
import com.example.app.helpers.IParseListener;
import com.example.app.helpers.ParseHelper;
import com.example.app.helpers.Prefs;
import com.example.app.models.DbModel;
import com.example.app.models.PhotoModel;
import com.example.app.services.interfaces.INetworkListener;

import java.io.File;
import java.nio.channels.NotYetConnectedException;
import java.util.List;

/**
 * Created by dev2 on 2/10/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper implements IParseListener, INetworkListener{

    public static Context context;

    private static DatabaseHelper instance;

    public static synchronized DatabaseHelper getInstance() {
        if (context == null){
            throw new NotYetConnectedException();
        }
        if (instance == null) {
            instance = new DatabaseHelper(context);
        }
        return instance;
    }

    private PhotoProcessor photoProcessor;
    private NetworkService networkService;

    private int namPhotosToRetrieve;
    private int retrievedPhotos;

    private DatabaseHelper(Context context) {
        super(context, DbModel.DB_name, null, 1);
        photoProcessor = PhotoProcessor.getInstance();
        networkService = NetworkService.getInstance();
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d(Prefs.LOG, "create database");

        sqLiteDatabase.execSQL("create table "+DbModel.Photos+" ("
                + PhotoModel.Id +" integer primary key autoincrement,"
                + PhotoModel.PhotoName +" text,"
                + PhotoModel.Latitude +" float,"
                + PhotoModel.Longitude +" float,"
                + PhotoModel.PhotoPath +" text,"
                + PhotoModel.ParsePath +" text,"
                + PhotoModel.IsDownloaded +" int"+ ");");

        networkService.retriveModels(this);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    public static ContentValues PhotoToCV(PhotoModel mPhoto){
        ContentValues cv;
        cv = new ContentValues();
        cv.put(PhotoModel.Latitude, mPhoto.latitude);
        cv.put(PhotoModel.Longitude, mPhoto.longitude);
        cv.put(PhotoModel.IsDownloaded, mPhoto.isDownloaded ? 1 : 0);
        cv.put(PhotoModel.PhotoPath, mPhoto.photoPath);
        cv.put(PhotoModel.PhotoName, mPhoto.photoName);
        return cv;
    }

    @Override
    public void photosObjectsRetrived(List<PhotoModel> photos) {
        Log.d(Prefs.LOG, "photo objects retrieved");
        namPhotosToRetrieve = photos.size();
        for (PhotoModel mPhoto:photos){
            photoProcessor.CreatePhoto(mPhoto, null);
            File photoLocation = new File(mPhoto.photoPath);
            if (photoLocation.exists())
                retrievedPhotos++;
            else
                networkService.retreivePhotoFromParse(mPhoto, this);
        }
    }

    @Override
    public void photoUploaded(Boolean error, String Path) {}

    @Override
    public void photoDownloaded(Boolean error) {
        Log.d(Prefs.LOG,"photo downloaded");
        retrievedPhotos++;
        if (retrievedPhotos == namPhotosToRetrieve){
            Toast.makeText(context, context.getResources().getString(R.string.photosRetreived), Toast.LENGTH_LONG);
        }
    }
}
