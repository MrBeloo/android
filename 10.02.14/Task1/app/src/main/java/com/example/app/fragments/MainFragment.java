package com.example.app.fragments;

import android.app.Activity;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.app.helpers.FileHelper;
import com.example.app.models.PhotoModel;
import com.example.app.helpers.Prefs;
import com.example.app.R;
import com.example.app.services.LocationFinder;
import com.example.app.services.PhotoProcessor;
import com.parse.ParseObject;

import java.io.File;

/**
 * Created by dev2 on 2/10/14.
 */
public class MainFragment extends Fragment  implements View.OnClickListener{

    public static final int PHOTO_ACTIVITY = 0;

    private PhotoProcessor photoProcessor;
    private PhotoModel newPhoto;
    private LocationFinder locationFinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        photoProcessor = PhotoProcessor.getInstance();
        locationFinder = LocationFinder.getInstance();

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button bu_newPhoto = (Button)view.findViewById(R.id.button_newPhoto);
        Button bu_viewPhoto = (Button)view.findViewById(R.id.button_viewPhoto);
        Button bu_viewMap = (Button)view.findViewById(R.id.button_viewMap);

        bu_newPhoto.setOnClickListener(this);
        bu_viewPhoto.setOnClickListener(this);
        bu_viewMap.setOnClickListener(this);

        locationFinder.startLocationTracking();

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        locationFinder.stopLocationTracking();
    }

    @Override
    public void onPause() {
        super.onPause();
        locationFinder.stopLocationTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationFinder.startLocationTracking();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(PhotoModel.Bundle,newPhoto);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState!=null)
            newPhoto = (PhotoModel)savedInstanceState.getSerializable(PhotoModel.Bundle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case PHOTO_ACTIVITY:
                    Location location = locationFinder.getCurrentLocation();
                    if (location!=null){
                        newPhoto.latitude = location.getLatitude();
                        newPhoto.longitude = location.getLongitude();
                    }
                    Log.d(Prefs.LOG, "before the db photo creation");

                    photoProcessor.CreatePhoto(newPhoto, null);

                    PhotoPreviewFragment photoPrFragment = new PhotoPreviewFragment();
                    Bundle b = new Bundle();
                    b.putSerializable(PhotoModel.Bundle,newPhoto);
                    photoPrFragment.setArguments(b);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, photoPrFragment)
                            .addToBackStack(null).commit();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_newPhoto:
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Uri photoPath = FileHelper.generateFileUri(FileHelper.createDirectory("temp"));
                File objFile = new File(photoPath.getPath());
                newPhoto = new PhotoModel();
                newPhoto.photoPath = photoPath.getPath();
                newPhoto.photoName = objFile.getName();
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoPath);
                startActivityForResult(intent, PHOTO_ACTIVITY);
                break;
            case R.id.button_viewPhoto:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container,new PhotoGalleryFragment())
                        .addToBackStack(null).commit();
                break;
            case R.id.button_viewMap:
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new GoogleMapsFragment())
                        .addToBackStack(null).commit();
                break;
        }

    }

}
