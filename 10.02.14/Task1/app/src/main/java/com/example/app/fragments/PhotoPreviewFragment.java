package com.example.app.fragments;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.app.helpers.ParseHelper;
import com.example.app.models.PhotoModel;
import com.example.app.helpers.Prefs;
import com.example.app.R;
import com.example.app.services.interfaces.INetworkListener;
import com.example.app.services.NetworkService;
import com.example.app.services.PhotoProcessor;

import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.widget.Toast;

/**
 * Created by dev2 on 2/10/14.
 */
public class PhotoPreviewFragment extends Fragment implements LoaderCallbacks<Cursor>, INetworkListener, View.OnClickListener{

    private static int PHOTO_LOADER = 1;

    PhotoProcessor photoProcessor;
    NetworkService networkService;

    volatile PhotoModel mPhoto;
    View view;
    ProgressDialog progressDialog;
    Button bu_upload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        photoProcessor = PhotoProcessor.getInstance();
        networkService = NetworkService.getInstance();

        view = inflater.inflate(R.layout.image_preview, container, false);

        bu_upload = (Button)view.findViewById(R.id.button_upload);
        ImageView imageView = (ImageView)view.findViewById(R.id.imageView_preview);

        Bundle b = getArguments();
        mPhoto = (PhotoModel)b.getSerializable(PhotoModel.Bundle);
        Log.d(Prefs.LOG, "preview " + mPhoto);

        Bitmap bitmap = BitmapFactory.decodeFile(mPhoto.photoPath);
        imageView.setImageBitmap(bitmap);

        getLoaderManager().initLoader(PHOTO_LOADER, null, this).forceLoad();

        bu_upload.setOnClickListener(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getResources().getString(R.string.loading));
        progressDialog.setMessage(getResources().getString(R.string.uploadDialogMessage));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        return view;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new PhotoProcessor.PhotoReaderByPath(getActivity(), mPhoto.photoPath);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.d(Prefs.LOG, "check photo finished");
        if (cursor.moveToFirst()){
            PhotoModel accepted = new PhotoModel(cursor);
            if (!accepted.isDownloaded){
                Log.d(Prefs.LOG, "this photo not uploaded");
                bu_upload.setEnabled(true);
                mPhoto = accepted;
            }
            cursor.close();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    @Override
    public void onClick(View view) {
        progressDialog.show();
        view.setEnabled(false);
        networkService.sendImageToParseCom(mPhoto.photoPath, mPhoto.photoName, this);
        Log.d(Prefs.LOG, "strore button clicked");
    }

    @Override
    public void photoUploaded(Boolean error, String url) {
        if (error){
            Log.d(Prefs.LOG,"error of upload");
            Toast.makeText(getActivity(), getResources().getString(R.string.uploadError), Toast.LENGTH_LONG).show();
            bu_upload.setEnabled(true);
            progressDialog.hide();
        } else {
            Log.d(Prefs.LOG, "before update photo" + mPhoto.toString());
            Toast.makeText(getActivity(), getResources().getString(R.string.uploadCorrect), Toast.LENGTH_LONG).show();
            mPhoto.parsePath = url;
            Log.d(Prefs.LOG,"photo from server " + mPhoto.parsePath);
            mPhoto.isDownloaded = true;
            networkService.storeModel(mPhoto);
            photoProcessor.updatePhotoById(mPhoto);
            progressDialog.hide();
        }
    }

    @Override
    public void photoDownloaded(Boolean error) {

    }


}
