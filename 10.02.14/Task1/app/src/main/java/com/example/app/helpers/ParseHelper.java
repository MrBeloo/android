package com.example.app.helpers;

import android.util.Log;

import com.example.app.models.PhotoModel;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev2 on 2/13/14.
 */
public class ParseHelper {

    public static PhotoModel modelFromParse(ParseObject object){
        PhotoModel mPhoto = new PhotoModel();
        mPhoto.photoPath = object.getString(PhotoModel.PhotoPath);
        mPhoto.isDownloaded = true;
        mPhoto.latitude = object.getDouble(PhotoModel.Latitude);
        mPhoto.longitude = object.getDouble(PhotoModel.Longitude);
        mPhoto.photoName = object.getString(PhotoModel.PhotoName);
        mPhoto.parsePath = object.getString(PhotoModel.ParsePath);
        return mPhoto;
    }


}
