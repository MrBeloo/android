package com.example.app.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.example.app.helpers.IParseListener;
import com.example.app.helpers.Prefs;
import com.example.app.models.PhotoModel;
import com.example.app.services.interfaces.INetworkListener;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev2 on 2/10/14.
 */
public class NetworkService {

    private static final NetworkService INSTANCE = new NetworkService();

    public static NetworkService getInstance(){
        return INSTANCE;
    }

    private class DonwloadFileThread extends Thread{

        private String fileUrl;
        private String filePath;
        private Handler handler;

        public DonwloadFileThread(String filePath, String fileUrl, Handler handler) {
            this.filePath = filePath;
            this.fileUrl = fileUrl;
            this.handler = handler;
        }

        public void run()
        {
            URL url;
            URLConnection conn;
            int fileSize;
            BufferedInputStream inStream;
            BufferedOutputStream outStream;
            File outFile;
            FileOutputStream fileStream;
            Message msg;
            Boolean error = false;
            try
            {
                url = new URL(fileUrl);
                conn = url.openConnection();
                conn.setUseCaches(false);
                fileSize = conn.getContentLength();

                inStream = new BufferedInputStream(conn.getInputStream());
                outFile = new File(filePath);
                fileStream = new FileOutputStream(outFile);
                outStream = new BufferedOutputStream(fileStream, fileSize);
                byte[] data = new byte[fileSize];
                int bytesRead = 0, totalRead = 0;
                while(!isInterrupted() && (bytesRead = inStream.read(data, 0, data.length)) >= 0)
                {
                    outStream.write(data, 0, bytesRead);
                }
                outStream.close();
                fileStream.close();
                inStream.close();
            }
            catch(MalformedURLException e)
            {
                error = true;
                Log.d(Prefs.LOG, "wrong url exception");
            }
            catch(FileNotFoundException e)
            {
                error = true;
                Log.d(Prefs.LOG, "file not found exception");
            }
            catch(IOException e)
            {
                error = true;
                Log.d(Prefs.LOG, "io exception");
            }

            handler.sendEmptyMessage(error ? 1:0);
        }
    }



    private class PostImageThread extends Thread{

        String url;
        String imagePath;
        List<NameValuePair> headers;
        Handler handler;

        public PostImageThread(String url, String imagePath, List<NameValuePair> headers, Handler handler){
            this.url = url;
            this.imagePath = imagePath;
            this.headers = headers;
            this.handler = handler;
        }

        @Override
        public void run() {
            Boolean error = false;
            String photoUrl="";
            try {
                JSONObject responce = new JSONObject(postImage(url, imagePath, headers));
                photoUrl = responce.getString("url");
            } catch (ClientProtocolException e) {
                error = true;
                Log.d(Prefs.LOG,"http exception");
            } catch (IOException e){
                error = true;
                Log.d(Prefs.LOG, "image read file exception");
            } catch (JSONException e){
                error = true;
                Log.d(Prefs.LOG, "wrong server responce");
            }
            Bundle b = new Bundle();
            b.putString("photoUrl", photoUrl);
            b.putBoolean("error", error);
            Message msg = new Message();
            msg.setData(b);
            handler.sendMessage(msg);
        }
    }

    private String postImage(String url, String imagePath, List<NameValuePair> headers) throws IOException{

        HttpParams params = new BasicHttpParams();
        params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
//        SingleClientConnManager mgr = new SingleClientConnManager(params, schemeRegistry);

        HttpClient client = new DefaultHttpClient(params);
        HttpPost httpPost = new HttpPost(url);

        //   MultipartEntity entity = new MultipartEntity(HttpMultipartMode.STRICT);

        for(int index=0; index < headers.size(); index++) {
            httpPost.setHeader(new BasicHeader(headers.get(index).getName(), headers.get(index).getValue()));
        }

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte [] ba = bao.toByteArray();
        httpPost.setEntity(new ByteArrayEntity(ba));

//            httpPost.setEntity(entity);

        ResponseHandler<String> responseHandler=new BasicResponseHandler();

        String responce = client.execute(httpPost, responseHandler);
//
        Log.d(Prefs.LOG, responce);

        return responce;
    }


    public void sendImageToParseCom(String imgPath, String imgName, final INetworkListener iListener){
        List<NameValuePair> body = new ArrayList<NameValuePair>();
        body.add(new BasicNameValuePair("image", imgPath));
        List<NameValuePair> headers = new ArrayList<NameValuePair>();
        headers.add(new BasicNameValuePair("X-Parse-Application-Id", "R1wrOHydOB0vkUNyFw7KO5hGQCaCjGXCEeJ8sZai"));
        headers.add(new BasicNameValuePair("X-Parse-REST-API-Key", "ClSf5pEgHpGnhzEPdqCqxuPPy0gvFHhewvjEzYfV"));
        headers.add(new BasicNameValuePair(HTTP.CONTENT_TYPE, "image/jpeg"));
        Handler h = new Handler(){
            public void handleMessage(Message msg) {
                Bundle b = msg.getData();
                iListener.photoUploaded(b.getBoolean("error"),b.getString("photoUrl"));
            }
        };
        Thread postImage = new PostImageThread("https://api.parse.com/1/files/"+imgName, imgPath, headers, h);
        postImage.start();
    }

    public void retreivePhotoFromParse(PhotoModel mPhoto,  final INetworkListener iListener){
        Thread downloadPhoto = new DonwloadFileThread(mPhoto.photoPath, mPhoto.parsePath, new Handler(){
            public void handleMessage(Message msg) {
                iListener.photoDownloaded(msg.what == 1);
            }
        });
        downloadPhoto.start();
    }


    public void storeModel(PhotoModel mPhoto) {
        class StoreModelThread extends Thread{
            PhotoModel mPhoto;

            public StoreModelThread(PhotoModel mPhoto){
                this.mPhoto = mPhoto;
            }

            @Override
            public void run(){
                try {

                    String url = "https://api.parse.com/1/classes/" + PhotoModel.class.getSimpleName();

                    HttpParams params = new BasicHttpParams();
                    params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                    HttpClient client = new DefaultHttpClient(params);

                    HttpPost httpPost = new HttpPost(url);

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.accumulate(PhotoModel.PhotoName, mPhoto.photoName);
                    jsonObject.accumulate(PhotoModel.Latitude, mPhoto.latitude);
                    jsonObject.accumulate(PhotoModel.Longitude, mPhoto.longitude);
                    jsonObject.accumulate(PhotoModel.ParsePath, mPhoto.parsePath);
                    jsonObject.accumulate(PhotoModel.PhotoPath, mPhoto.photoPath);

                    String json = jsonObject.toString();
                    StringEntity se = new StringEntity(json);
                    httpPost.setEntity(se);

                    httpPost.setHeader("Accept", "application/json");
                    httpPost.setHeader("Content-type", "application/json");
                    httpPost.setHeader("X-Parse-Application-Id", "R1wrOHydOB0vkUNyFw7KO5hGQCaCjGXCEeJ8sZai");
                    httpPost.setHeader("X-Parse-REST-API-Key", "ClSf5pEgHpGnhzEPdqCqxuPPy0gvFHhewvjEzYfV");

                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    String responce = client.execute(httpPost, responseHandler);
                    Log.d(Prefs.LOG, responce);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                }
            }
        }

        new StoreModelThread(mPhoto).start();
    }

    public void retriveModels(IParseListener listener) {
        try {
            HttpParams params = new BasicHttpParams();
            params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpClient client = new DefaultHttpClient(params);

            String url = "https://api.parse.com/1/classes/" + PhotoModel.class.getSimpleName();

            HttpGet get = new HttpGet(url);
            get.setHeader("Accept", "application/json");
            get.setHeader("Content-type", "application/json");
            get.setHeader("X-Parse-Application-Id", "R1wrOHydOB0vkUNyFw7KO5hGQCaCjGXCEeJ8sZai");
            get.setHeader("X-Parse-REST-API-Key", "ClSf5pEgHpGnhzEPdqCqxuPPy0gvFHhewvjEzYfV");

            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            String responce = client.execute(get, responseHandler);

            JSONArray jsonArray = (new JSONObject(responce)).getJSONArray("results");

            List<PhotoModel> photoModels = new ArrayList<PhotoModel>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                PhotoModel mPhoto = new PhotoModel();
                mPhoto.isDownloaded = true;
                mPhoto.latitude = jsonObject.getLong(PhotoModel.Latitude);
                mPhoto.longitude = jsonObject.getLong(PhotoModel.Longitude);
                mPhoto.parsePath = jsonObject.getString(PhotoModel.ParsePath);
                mPhoto.photoName = jsonObject.getString(PhotoModel.PhotoName);
                mPhoto.photoPath = jsonObject.getString(PhotoModel.PhotoPath);
                photoModels.add(mPhoto);
            }

            listener.photosObjectsRetrived(photoModels);

            Log.d(Prefs.LOG, responce);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}