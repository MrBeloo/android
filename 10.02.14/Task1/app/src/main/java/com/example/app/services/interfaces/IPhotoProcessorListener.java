package com.example.app.services.interfaces;

/**
 * Created by dev2 on 2/10/14.
 */
public interface IPhotoProcessorListener {
    public void photoStored();
}
