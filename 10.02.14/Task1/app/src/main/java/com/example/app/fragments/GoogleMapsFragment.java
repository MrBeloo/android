package com.example.app.fragments;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.app.helpers.Prefs;
import com.example.app.R;
import com.example.app.models.PhotoModel;
import com.example.app.services.LocationFinder;
import com.example.app.services.PhotoProcessor;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Hashtable;

/**
 * Created by dev2 on 2/12/14.
 */
public class GoogleMapsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private static int PHOTOS_LOADER = 5;

    GoogleMap map;
    SupportMapFragment gmapsFragment;
    LinearLayout layout;
    LocationFinder locationFinder;
    Hashtable<String, PhotoModel> photos;

    public GoogleMapsFragment() {
        photos = new Hashtable<String, PhotoModel>();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.googlemaps_static_layout, container, false);
        layout = (LinearLayout)view.findViewById(R.id.gMaps_layout);

        locationFinder = LocationFinder.getInstance();
        locationFinder.startLocationTracking();

        gmapsFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
        map = gmapsFragment.getMap();

        if (map == null) {
            TextView tb = new TextView(getActivity());
            tb.setText(getResources().getString(R.string.noGmaps));
            tb.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            layout.removeAllViews();
            layout.addView(tb);
        } else {
            Log.d(Prefs.LOG, "map created");
            Location location = locationFinder.getCurrentLocation();
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
                            location.getLongitude()));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(17);

            map.moveCamera(center);
            map.animateCamera(zoom);
            map.setInfoWindowAdapter(new InfoViewAdapter());
            map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    PhotoModel mPhoto = photos.get(marker.getId());
                    PhotoPreviewFragment photoFragment = new PhotoPreviewFragment();
                    Bundle b = new Bundle();
                    b.putSerializable(PhotoModel.Bundle,mPhoto);
                    photoFragment.setArguments(b);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, photoFragment)
                            .addToBackStack(null).commit();
                }
            });
            map.setMyLocationEnabled(true);
        }

        getLoaderManager().initLoader(PHOTOS_LOADER, null, this);

        return view;
    }

    private class InfoViewAdapter implements GoogleMap.InfoWindowAdapter{
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                PhotoModel mPhoto = photos.get(marker.getId());
                Log.d(Prefs.LOG, mPhoto.photoPath);
                View view = getActivity().getLayoutInflater().inflate(R.layout.info_photo_marker_layout, null, false);
                ImageView imageView = (ImageView)view.findViewById(R.id.photo);
                TextView tv = (TextView)view.findViewById(R.id.title);
                tv.setText(mPhoto.photoName);
//                ImageLoader imageLoader = ImageLoader.getInstance();
//                imageLoader.displayImage("file:/" + mPhoto.photoPath, imageView);
                Bitmap bitmap = BitmapFactory.decodeFile(mPhoto.photoPath);
                imageView.setImageBitmap(bitmap);

                Log.d(Prefs.LOG,"marker drew");
                return view;
            }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportFragmentManager().beginTransaction()
                .remove(gmapsFragment).commit();
    }

    @Override
    public void onStop() {
        super.onStop();
        locationFinder.stopLocationTracking();
    }

    @Override
    public void onPause() {
        super.onPause();
        locationFinder.stopLocationTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        locationFinder.startLocationTracking();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new PhotoProcessor.CursorReader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor.moveToFirst())
            do {
                PhotoModel mPhoto = new PhotoModel(cursor);
                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(mPhoto.latitude, mPhoto.longitude))
                        .title(mPhoto.photoName));
                photos.put(marker.getId(), mPhoto);
            } while (cursor.moveToNext());

    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }
}
