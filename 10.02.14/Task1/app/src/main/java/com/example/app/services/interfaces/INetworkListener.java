package com.example.app.services.interfaces;

/**
 * Created by dev2 on 2/11/14.
 */
public interface INetworkListener {
    public void photoUploaded(Boolean error, String Path);
    public void photoDownloaded (Boolean error);
}
