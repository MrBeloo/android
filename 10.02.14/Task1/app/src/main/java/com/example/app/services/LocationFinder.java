package com.example.app.services;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.example.app.helpers.Prefs;

import java.nio.channels.NotYetConnectedException;

/**
 * Created by dev2 on 2/12/14.
 */
public class LocationFinder implements LocationListener {

    private static LocationFinder instance;
    public static Context context;

    public static synchronized LocationFinder getInstance() {
        if (context == null){
            throw new NotYetConnectedException();
        }
        if (instance == null) {
            instance = new LocationFinder();
        }
        return instance;
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    private Location lastKnownLocation;
    private LocationManager locationManager;
    private int controllers;

    private LocationFinder(){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void startLocationTracking(){
        controllers++;
        lastKnownLocation = getBetterLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER),
                locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 100, this);
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 100, this);
        }
    }

    public void stopLocationTracking(){
        controllers--;
        if (controllers == 0)
            locationManager.removeUpdates(this);
    }


    public Location getCurrentLocation(){
        return lastKnownLocation;
    }

    @Override
    public void onLocationChanged(Location location) {

        lastKnownLocation = getBetterLocation(location, lastKnownLocation);
        Log.d(Prefs.LOG, "on Location changed, lat =" + lastKnownLocation.getLatitude() +" long = " + lastKnownLocation.getLongitude());
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private  boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }
        if (location == null){
            return false;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private Location getBetterLocation(Location firstLocation, Location secondLocation) {
        if (isBetterLocation(firstLocation,secondLocation))
            return firstLocation;
        else return secondLocation;
    }
}
