package com.example.app.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.app.models.PhotoModel;
import com.example.app.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;

import java.net.URI;

/**
 * Created by dev2 on 03.02.14.
 */

public class PhotoCursorAdapter extends CursorAdapter{


    private class ViewHolder{
        ImageView imageView;
        DisplayImageOptions options;
        ProgressBar imageLoadProgress;
    }

    public PhotoCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        lInflater = (LayoutInflater.from(context));
    }

    LayoutInflater lInflater;

    @Override
    public void bindView(View view, Context context, Cursor cursor) {}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return lInflater.inflate(R.layout.grid_item, viewGroup,false);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = lInflater.inflate(R.layout.grid_item, parent, false);

            viewHolder.imageView = (ImageView)convertView.findViewById(R.id.imageView_photo);
            viewHolder.imageLoadProgress = (ProgressBar)convertView.findViewById(R.id.progress);

            viewHolder.options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.ic_stub)
                    .showImageForEmptyUri(R.drawable.ic_empty)
                    .showImageOnFail(R.drawable.ic_error)
                    .cacheInMemory(true)
                    .cacheOnDisc(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Cursor cursor = (Cursor)getItem(position);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage("file:/"+cursor.getString(cursor.getColumnIndex(PhotoModel.PhotoPath)), viewHolder.imageView,
                viewHolder.options,
                new SimpleImageLoadingListener(){
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        viewHolder.imageLoadProgress.setProgress(0);
                        viewHolder.imageLoadProgress.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        viewHolder.imageLoadProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        viewHolder.imageLoadProgress.setVisibility(View.GONE);
                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current,
                                                 int total) {
                        viewHolder.imageLoadProgress.setProgress(Math.round(100.0f * current / total));
                    }
                });

        return convertView;
    }


}

