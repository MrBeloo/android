package com.example.app.helpers;

import android.net.Uri;
import android.os.Environment;

import java.io.File;

/**
 * Created by dev2 on 2/10/14.
 */
public class FileHelper {

    public static Uri generateFileUri(File directory) {
        File file = new File(directory.getPath() + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        return Uri.fromFile(file);
    }

    public static File createDirectory(String dirPath) {
        File directory = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),dirPath);
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }
}
