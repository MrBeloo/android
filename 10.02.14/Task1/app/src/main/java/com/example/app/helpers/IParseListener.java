package com.example.app.helpers;

import com.example.app.models.PhotoModel;

import java.util.List;

/**
 * Created by dev2 on 2/13/14.
 */
public interface IParseListener {
    public void photosObjectsRetrived(List<PhotoModel> photos);
}
